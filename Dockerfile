FROM docker.io/library/eclipse-temurin:17-alpine
EXPOSE 8080
ADD backend/build/distributions/kitty.tgz /app
ENV STATIC_ROOT_DIR=/app/kitty/share/frontend
CMD ["/app/kitty/bin/kitty"]
