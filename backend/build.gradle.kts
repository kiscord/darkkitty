import org.jetbrains.kotlin.gradle.targets.js.webpack.*

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    application
    `jvm-test-suite`
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

fun JavaToolchainSpec.kittyToolchain() {
    languageVersion.set(JavaLanguageVersion.of(17))
}

kotlin {
    target {
        compilations.configureEach {
            kotlinOptions {
                jvmTarget = "17"
                freeCompilerArgs = freeCompilerArgs + "-Xcontext-receivers"
            }
        }
    }

    sourceSets.configureEach {
        languageSettings {
            optIn("kotlin.RequiresOptIn")
            progressiveMode = true
        }
    }

    jvmToolchain {
        kittyToolchain()
    }
}

java {
    toolchain {
        kittyToolchain()
    }
}

dependencies {
    api(project(":shared"))

    api(kotlin("stdlib-jdk8"))
    api(kiscord.kiscord.core)
    api(kiscord.kiscord.gateway.events)
    api(kiscord.kiscord.gateway.interactions)
    api(kiscord.kiscord.interactions)
    api(kiscord.kiscord.interactions.server)
    api(kiscord.kotlinx.coroutines.core)
    api(kiscord.ktor.client.logging)
    api(kiscord.ktor.server.core)
    api(kiscord.slf4j.api)
    api(libs.bundles.bouncycastle)
    api(libs.bundles.exposed)
    api(libs.bundles.flyway)
    api(libs.bundles.kodein.backend)
    api(libs.bundles.kubernetes)
    api(libs.hikari.cp)
    api(libs.jsoup)

    api("io.ktor:ktor-server-auth-jwt")
    api("io.ktor:ktor-server-call-logging")
    api("io.ktor:ktor-server-compression")
    api("io.ktor:ktor-server-compression-jvm")
    api("io.ktor:ktor-server-content-negotiation")
    api("io.ktor:ktor-server-default-headers")
    api("io.ktor:ktor-server-forwarded-header")
    api("io.ktor:ktor-server-host-common")
    api("io.ktor:ktor-server-partial-content")
    api("io.ktor:ktor-server-request-validation")
    api("io.ktor:ktor-server-resources")
    api("io.ktor:ktor-server-status-pages")

    implementation(libs.okio)
    implementation("io.ktor:ktor-server-cio")
    implementation("org.slf4j:jul-to-slf4j:${kiscord.versions.slf4j.get()}")

    runtimeOnly(kiscord.kotlinx.coroutines.debug)
    runtimeOnly(kiscord.ktor.client.cio)
    runtimeOnly(libs.bundles.jdbc)

    implementation(platform(kiscord.kotlin.bom))
    implementation(platform(kiscord.junit5.bom))
    implementation(platform(libs.test.containers.bom))
}

application {
    applicationName = "kitty"
    mainClass.set("kitty.backend.Kitty")
}

tasks.named<Jar>("jar").configure {
    manifest.attributes("Main-Class" to application.mainClass.get())
}

tasks.named<JavaExec>("run").configure {
    args("-P:ktor.development=true")
    environment("BOT_PORT" to ext["kitty.backend.port"].toString())
    javaLauncher.set(javaToolchains.launcherFor {
        kittyToolchain()
    })
}

testing {
    suites {
        withType<JvmTestSuite>().configureEach {
            useJUnitJupiter()

            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-test-junit5")
                implementation("org.junit.jupiter:junit-jupiter")
                implementation("org.junit.jupiter:junit-jupiter-engine")
                implementation("org.junit.jupiter:junit-jupiter-params")
                implementation(project(":test"))
                runtimeOnly(kiscord.logback.classic)
                runtimeOnly(libs.jansi)
            }
        }

        val databaseTest by registering(JvmTestSuite::class) {
            testType.set(TestSuiteType.INTEGRATION_TEST)

            dependencies {
                implementation(project())
                implementation(libs.test.containers.junit.jupiter)
                implementation.bundle(libs.bundles.test.containers)
                implementation.bundle(libs.bundles.jdbc)
            }
        }
    }
}

tasks.named("check") {
    dependsOn(testing.suites.named("databaseTest"))
}

evaluationDependsOn(":frontend")

distributions {
    main {
        contents {
            into("share/frontend") {
                from(project(":frontend").tasks.named<KotlinWebpack>("browserProductionWebpack").map {
                    fileTree(it.destinationDirectory).builtBy(it)
                })
            }
        }
    }
}

tasks.distTar.configure {
    archiveVersion.convention(null as String?)
    compression = Compression.GZIP
}

tasks.distZip.configure {
    archiveVersion.convention(null as String?)
}
