package kitty.backend.test.db

import kitty.*
import kitty.backend.db.*
import kitty.test.*
import org.flywaydb.core.api.*
import org.kodein.di.*
import org.kodein.log.frontend.*

val DatabaseTestModule = DI.Module("kitty-database-test") {
    importOnce(KittyCommonModule)
    importOnce(TestModule)
    importOnce(DatabaseModule)

    bindConstant(tag = "kitty-database-test-database-name") { BuildConfig.NAME.lowercase() }

    inBindSet { add { provider { slf4jFrontend } } }

    inBindSet(tag = "flyway-migrations") {
        if (BuildConfig.MODULE_FAQ) add {
            singleton {
                Location("classpath:kitty/backend/modules/faq/db/migrations")
            }
        }

        if (BuildConfig.MODULE_PASTA) add {
            singleton {
                Location("classpath:kitty/backend/modules/pasta/db/migrations")
            }
        }
    }
}
