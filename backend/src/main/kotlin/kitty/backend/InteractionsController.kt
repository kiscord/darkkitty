package kitty.backend

import kiscord.interactions.*
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.*
import org.kodein.di.*

abstract class AbstractInteractionsController(override val di: DI) : InteractionsController {
    private val database: Database by instance()

    suspend fun <T> withTransaction(block: suspend Transaction.() -> T): T {
        return newSuspendedTransaction(context = Dispatchers.IO, db = database, statement = block)
    }
}

interface InteractionsController : DIAware {
    fun InteractionsHost.Builder.install()
}