package kitty.backend

import org.kodein.di.*
import io.ktor.server.application.Application as KtorApplication

interface KtorConfigurator : DIAware {
    fun KtorApplication.configure()
}

abstract class AbstractKtorConfigurator(override val di: DI) : KtorConfigurator