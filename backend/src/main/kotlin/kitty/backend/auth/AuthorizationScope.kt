package kitty.backend.auth

import io.ktor.server.auth.*
import kiscord.builder.*
import kotlin.coroutines.*

@RestrictsSuspension
interface AuthorizationScope<in C : AuthorizationScope.Context> {
    fun check(context: C, principal: Principal): Boolean

    interface Context {
        suspend fun retrievePermissions(principal: Principal): Set<Permission>

        object Empty : Context {
            override suspend fun retrievePermissions(principal: Principal): Set<Permission> = emptySet()
        }
    }

    @JvmInline
    value class Permission(
        val name: String,
    )

    interface Builder<C : Context> : BuilderFor<AuthorizationScope<C>> {
        fun require(block: suspend (C, Principal) -> Boolean)
        fun require(permission: Permission)

        fun requireAll(permissions: Collection<Permission>) = and {
            permissions.forEach(::require)
        }

        fun requireAny(permissions: Collection<Permission>) = or {
            permissions.forEach(::require)
        }

        fun <R : Context> or(context: R, block: Builder<R>.() -> Unit)
        fun <R : Context> and(context: R, block: Builder<R>.() -> Unit)
        fun <R : Context> or(context: (C) -> R, block: Builder<R>.() -> Unit)
        fun <R : Context> and(context: (C) -> R, block: Builder<R>.() -> Unit)
        fun or(block: Builder<C>.() -> Unit)
        fun and(block: Builder<C>.() -> Unit)
    }
}
