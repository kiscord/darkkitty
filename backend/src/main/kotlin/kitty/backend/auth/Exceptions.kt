package kitty.backend.auth

class ForbiddenException(message: String = "Access to the resource is forbidden") : Exception(message)
class UnauthorizedException(message: String = "Authorization required") : Exception(message)