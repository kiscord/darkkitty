package kitty.backend.auth

import com.auth0.jwk.*
import kotlinx.serialization.*

@Serializable
data class Jwks(
    val keys: List<@Serializable(with = JwkSerializer::class) Jwk>
)
