package kitty.backend.auth

import com.auth0.jwt.interfaces.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import kiscord.api.*

class KittyPrincipal(payload: Payload) : JWTPayloadHolder(payload), Principal {
    val provider: String get() = payload.getClaim(CLAIM_PROVIDER).asString()
    val discordUserId: Snowflake get() = Snowflake(payload.getClaim(CLAIM_DISCORD).asMap()["user_id"] as String)
    val discordAccessToken: String? get() = payload.getClaim(CLAIM_DISCORD).asMap()["access_token"] as String?
    val discordRefreshToken: String? get() = payload.getClaim(CLAIM_DISCORD).asMap()["refresh_token"] as String?

    companion object {
        const val CLAIM_DISCORD = "discord"
        const val CLAIM_PROVIDER = "provider"
        const val JWKS_PATH = "/.well-known/jwks.json"
    }
}