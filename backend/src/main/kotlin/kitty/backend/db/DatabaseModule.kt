package kitty.backend.db

import kitty.backend.*
import org.flywaydb.core.*
import org.flywaydb.core.api.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.*
import org.kodein.di.*

val DatabaseModule = DI.Module("kitty-database") {
    bindSingleton {
        Database.connect(datasource = instance())
    }

    bindSet<Table>(tag = "tables-in-use")

    bindSet<Location>(tag = "flyway-migrations")
    inBindSet(tag = "flyway-migrations") { add { singleton { Location("classpath:kitty/backend/db/migrations") } } }
    bindSet<Pair<String, String>>(tag = "flyway-placeholders")
    bindSet<FlywayPlaceholdersProvider>(tag = "flyway-placeholder-providers")
    inBindSet<FlywayPlaceholdersProvider>(tag = "flyway-placeholder-providers") {
        add { provider { FlywayDataTypePlaceholders } }
    }

    bindSingleton {
        FlywayLogCreator.loggerFactoryLocal.set(instance())

        val config = instanceOrNull<KittyConfig>()
        val locations = instance<Set<Location>>(tag = "flyway-migrations")
        val placeholders = instance<Set<Pair<String, String>>>(tag = "flyway-placeholders").toMap()
        val placeholderProviders = instance<Set<FlywayPlaceholdersProvider>>(tag = "flyway-placeholder-providers")

        val database = instance<Database>()
        val computedPlaceholders = transaction(db = database) {
            placeholderProviders.fold(placeholders) { acc, provider ->
                acc + provider.providePlaceholders()
            }
        }

        val flyway = Flyway.configure()
            .dataSource(instance())
            .outOfOrder(true)
            .locations(*locations.sorted().toTypedArray())
            .validateMigrationNaming(true)
            .loggers(FlywayLogCreator::class.qualifiedName)
            .placeholders(computedPlaceholders)
            .executeInTransaction(true)

        config?.database?.baselineVersion?.let { baselineVersion ->
            flyway.baselineOnMigrate(true).baselineVersion(baselineVersion)
        }

        flyway.load()
    }
}
