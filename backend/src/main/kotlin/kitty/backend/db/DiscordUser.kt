package kitty.backend.db

import kitty.*
import kiscord.api.*
import kotlinx.datetime.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.kotlin.datetime.*
import kotlin.reflect.full.*
import kiscord.api.Locale as DiscordLocale
import kiscord.api.User as KiscordUser

object DiscordUsers : SnowflakeIdTable() {
    val username = varchar("username", 32)
    val discriminator = varchar("discriminator", 4)
    val avatar = varchar("avatar", 34).nullable()
    val mfaEnabled = bool("mfa_enabled").nullable()
    val banner = varchar("banner", 32).nullable()
    val accentColor = integer("accent_color").nullable()
    @OptIn(KiscordUnstableAPI::class)
    val locale = customEnumeration("locale", sql = "VARCHAR(6)",
        fromDb = { DiscordLocale.valueToEnum(it as String) ?: DiscordLocale.EnglishUS },
        toDb = { DiscordLocale.enumToValue(it) }).nullable()
    val verified = bool("verified").nullable()
    val email = varchar("email", 36).nullable()
    val flags = integer("flags").nullable()
    val publicFlags = integer("public_flags").nullable()
    val bot = bool("bot").nullable()
    val updatedAt = timestamp("updated_at").defaultExpression(CurrentTimestamp())
}

class DiscordUser(id: EntityID<Snowflake>) : SnowflakeEntity(id) {
    var username by DiscordUsers.username
    var discriminator by DiscordUsers.discriminator
    var avatar by DiscordUsers.avatar
    var mfaEnabled by DiscordUsers.mfaEnabled
    var banner by DiscordUsers.banner
    var accentColor by DiscordUsers.accentColor
    var locale by DiscordUsers.locale
    var verified by DiscordUsers.verified
    var email by DiscordUsers.email
    var flags by DiscordUsers.flags.transform(UserFlagsTransformer)
    var publicFlags by DiscordUsers.publicFlags.transform(UserFlagsTransformer)
    var bot by DiscordUsers.bot
    var updatedAt by DiscordUsers.updatedAt

    companion object : SnowflakeEntityClass<DiscordUser>(DiscordUsers) {
        fun updateUser(user: KiscordUser): DiscordUser {
            val mapped = findById(user.id) ?: return new(user.id) { takeFrom(user) }
            mapped.takeFrom(user)
            mapped.updatedAt = Clock.System.now()
            return mapped
        }
    }

    fun asProfile(): UserProfile = UserProfile(
        id = id.value,
        username = username,
        discriminator = discriminator,
        avatar = avatar,
        mfaEnabled = mfaEnabled,
        verified = verified,
        flags = flags,
        bot = bot,
    )
}

fun DiscordUser.takeFrom(user: KiscordUser) {
    username = user.username
    discriminator = user.discriminator
    avatar = user.avatar
    if (user.mfaEnabled != null) mfaEnabled = user.mfaEnabled
    if (user.locale != null) locale = user.locale
    if (user.verified != null) verified = user.verified
    if (user.email.isPresent) email = user.email.orNull
    if (user.flags != null) flags = user.flags
    if (user.publicFlags != null) publicFlags = user.publicFlags
    if (user.bot != null) bot = user.bot
}
