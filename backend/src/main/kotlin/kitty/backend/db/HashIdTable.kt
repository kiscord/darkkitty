package kitty.backend.db

import kitty.*
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*

class HashIdColumnType(hashSize: Int = HashId.DEFAULT_HASH_SIZE) : IColumnType by CharColumnType(hashSize) {
    override fun validateValueBeforeUpdate(value: Any?) {
        if (value is HashId) {
            super.validateValueBeforeUpdate(value.hash)
        } else {
            super.validateValueBeforeUpdate(value)
        }
    }

    override fun notNullValueToDB(value: Any): Any = when (value) {
        is HashId -> super.notNullValueToDB(value.hash)
        else -> super.notNullValueToDB(value)
    }

    override fun valueFromDB(value: Any): Any = when (val raw = super.valueFromDB(value)) {
        is String -> HashId(raw)
        else -> raw
    }
}

fun Table.hashId(name: String, hashSize: Int = HashId.DEFAULT_HASH_SIZE): Column<HashId> {
    return registerColumn(name, HashIdColumnType(hashSize))
}

open class HashIdTable(name: String = "", columnName: String = "id", hashSize: Int = HashId.DEFAULT_HASH_SIZE) :
    IdTable<HashId>(name) {
    final override val id: Column<EntityID<HashId>> = hashId(columnName, hashSize).clientDefault {
        HashId.generate(hashSize)
    }.entityId()
    final override val primaryKey = PrimaryKey(id)
}

abstract class HashIdEntity(id: EntityID<HashId>) : Entity<HashId>(id)

abstract class HashIdEntityClass<out E : HashIdEntity>(
    table: IdTable<HashId>,
    entityType: Class<E>? = null,
    entityCtor: ((EntityID<HashId>) -> E)? = null
) : EntityClass<HashId, E>(table, entityType, entityCtor)
