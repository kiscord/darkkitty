package kitty.backend.db

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.wrap
import org.jetbrains.exposed.sql.statements.api.*
import org.jetbrains.exposed.sql.statements.jdbc.*
import org.jetbrains.exposed.sql.transactions.*
import org.jetbrains.exposed.sql.vendors.*
import java.sql.Array as SqlArray

interface ArrayLikeColumnType : IColumnType {
    val itemType: IColumnType
}

class ListColumnType(override val itemType: IColumnType, val maximumCardinality: Int? = null) : ColumnType(),
    ArrayLikeColumnType {
    override fun sqlType(): String = buildString {
        append(itemType.sqlType())
        append(" ARRAY")
        if (maximumCardinality != null) {
            append('[')
            append(maximumCardinality)
            append(']')
        }
    }

    override fun valueFromDB(value: Any): Any = when (value) {
        is SqlArray -> valueFromDB(value.array)
        is Array<*> -> value.map { it?.let { itemType.valueFromDB(it) } }
        is Iterable<*> -> value.map { it?.let { itemType.valueFromDB(it) } }
        else -> error("Unexpected value of type Array: $value of ${value::class.qualifiedName}")
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> Any.mapToList(): List<T> = when (this) {
        is SqlArray -> (array as Array<out T>).asList()
        is Array<*> -> (this as Array<out T>).asList()
        is List<*> -> this as List<T>
        is Iterable<*> -> (this as Iterable<T>).toList()
        else -> error("Unexpected value of type Array: $this of ${this::class.qualifiedName}")
    }

    override fun notNullValueToDB(value: Any): Any = when (value) {
        is SqlArray -> notNullValueToDB(value)
        else -> {
            val list = value.mapToList<Any?>()
            val connection = (TransactionManager.current().connection as JdbcConnectionImpl).connection
            connection.createArrayOf(itemType.sqlType(), Array(list.size) { idx ->
                itemType.valueToDB(list[idx])
            })
        }
    }

    override fun setParameter(stmt: PreparedStatementApi, index: Int, value: Any?) {
        if (value == null) {
            stmt.setNull(index, this)
            return
        }

        check(stmt is JdbcPreparedStatementImpl) { "Only JDBC connections supported" }

        val list = value.mapToList<Any?>()
        val connection = stmt.statement.connection
        val array = connection.createArrayOf(itemType.sqlType(), Array(list.size) { idx ->
            itemType.valueToDB(list[idx])
        })

        stmt.statement.setArray(index, array)
    }

    override fun valueToString(value: Any?): String {
        if (value == null) {
            check(nullable) { "NULL in non-nullable column" }
            return "NULL"
        }
        if (value is LiteralOp<*> && value.columnType == this) {
            return valueToString(value.value)
        }
        return value.mapToList<Any?>().joinToString(prefix = "ARRAY[", separator = ",", postfix = "]") {
            itemType.valueToString(it)
        }
    }
}

fun <T> Table.list(name: String, type: IColumnType, maximumCardinality: Int? = null): Column<List<T>> =
    registerColumn(name, ListColumnType(type, maximumCardinality))

class ArrayIndex<T>(
    private val expr: Expression<out List<T>>,
    private val index: Int,
    columnType: ArrayLikeColumnType
) : ExpressionWithColumnType<T>() {
    override val columnType: IColumnType = columnType.itemType

    override fun toQueryBuilder(queryBuilder: QueryBuilder) {
        expr.toQueryBuilder(queryBuilder)
        queryBuilder.append('[')
        queryBuilder.registerArgument(IntegerColumnType(), index)
        queryBuilder.append(']')
    }
}

operator fun <T> ExpressionWithColumnType<out List<T>>.get(index: Int): ExpressionWithColumnType<T> {
    val columnType = columnType
    check(columnType is ArrayLikeColumnType) { "Only ArrayLikeColumnType can be used" }
    return ArrayIndex(this, index, columnType)
}

class ContainsOp<T>(val array: Expression<out List<T>>, val item: Expression<in T>) : Op<Boolean>() {
    override fun toQueryBuilder(queryBuilder: QueryBuilder) {
        when(currentDialect) {
            is H2Dialect -> {
                queryBuilder.append("ARRAY_CONTAINS(").append(array).append(", ").append(item).append(')')
            }
            else -> {
                queryBuilder.append(array).append(" @> ARRAY[").append(item).append("]")
            }
        }
    }
}

infix fun <T> ExpressionWithColumnType<out List<T>>.contains(expr: Expression<in T>): Op<Boolean> {
    return ContainsOp(this, expr)
}

infix fun <T> ExpressionWithColumnType<out List<T>>.contains(value: T): Op<Boolean> {
    val columnType = columnType
    check(columnType is ArrayLikeColumnType) { "Only ArrayLikeColumnType can be used" }
    return contains(LiteralOp(columnType.itemType, value))
}

class AnyOp<T>(expr1: Expression<out List<T>>, expr2: Expression<out List<T>>) : ComparisonOp(expr1, expr2, "@>")

fun <T> ExpressionWithColumnType<out List<T>>.any(items: Expression<out List<T>>): Op<Boolean> = AnyOp(this, items)
fun <T> ExpressionWithColumnType<out List<T>>.any(items: Iterable<T>): Op<Boolean> = any(LiteralOp(columnType, items))
fun <T> ExpressionWithColumnType<out List<T>>.any(vararg items: T): Op<Boolean> = any(items.asList())
infix fun <T> ExpressionWithColumnType<out List<T>>.any(item: T): Op<Boolean> = any(listOf(item))

class AllOp<T>(expr1: Expression<out List<T>>, expr2: Expression<out List<T>>) : ComparisonOp(expr1, expr2, "&&")

fun <T> ExpressionWithColumnType<out List<T>>.all(items: Expression<out List<T>>): Op<Boolean> = AllOp(this, items)
fun <T> ExpressionWithColumnType<out List<T>>.all(items: Iterable<T>): Op<Boolean> = all(LiteralOp(columnType, items))
fun <T> ExpressionWithColumnType<out List<T>>.all(vararg items: T): Op<Boolean> = all(items.asList())
infix fun <T> ExpressionWithColumnType<out List<T>>.all(item: T): Op<Boolean> = all(listOf(item))
