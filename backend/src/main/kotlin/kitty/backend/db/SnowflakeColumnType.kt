package kitty.backend.db

import kiscord.api.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.api.*
import org.jetbrains.exposed.sql.vendors.*

class SnowflakeColumnType : ColumnType() {
    override fun sqlType(): String = currentDialect.dataTypeProvider.ulongType()

    override fun valueFromDB(value: Any): Snowflake {
        return when (value) {
            is Snowflake -> value
            is ULong -> Snowflake(value)
            is Long -> value.takeIf { it >= 0 }?.let { Snowflake(it.toULong()) }
            is Number -> value.toLong().takeIf { it >= 0 }?.let { Snowflake(it.toULong()) }
            is String -> Snowflake(value.toULong())
            else -> error("Unexpected value of type Snowflake: $value of ${value::class.qualifiedName}")
        } ?: error("negative value but type is Snowflake: $value")
    }

    override fun setParameter(stmt: PreparedStatementApi, index: Int, value: Any?) {
        val v = if (value is Snowflake) value.id.toLong() else value
        super.setParameter(stmt, index, v)
    }

    override fun notNullValueToDB(value: Any): Any {
        val v = if (value is Snowflake) value.id.toLong() else value
        return super.notNullValueToDB(v)
    }
}

fun Table.snowflake(name: String) = registerColumn<Snowflake>(name, SnowflakeColumnType())
