package kitty.backend.db

import kiscord.api.*
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*

open class SnowflakeIdTable(name: String = "", columnName: String = "id") : IdTable<Snowflake>(name) {
    final override val id: Column<EntityID<Snowflake>> = snowflake(columnName).entityId()
    final override val primaryKey = PrimaryKey(id)
}

abstract class SnowflakeEntity(id: EntityID<Snowflake>) : Entity<Snowflake>(id)

abstract class SnowflakeEntityClass<out E : SnowflakeEntity>(
    table: IdTable<Snowflake>,
    entityType: Class<E>? = null,
    entityCtor: ((EntityID<Snowflake>) -> E)? = null
) : EntityClass<Snowflake, E>(table, entityType, entityCtor)
