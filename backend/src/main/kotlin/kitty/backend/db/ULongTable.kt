package kitty.backend.db

import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*

@ExperimentalUnsignedTypes
open class ULongIdTable(name: String = "", columnName: String = "id") : IdTable<ULong>(name) {
    final override val id: Column<EntityID<ULong>> = ulong(columnName).autoIncrement().entityId()
    final override val primaryKey = PrimaryKey(id)
}

abstract class ULongEntity(id: EntityID<ULong>) : Entity<ULong>(id)

abstract class ULongEntityClass<out E : ULongEntity>(
    table: IdTable<ULong>,
    entityType: Class<E>? = null,
    entityCtor: ((EntityID<ULong>) -> E)? = null
) : EntityClass<ULong, E>(table, entityType, entityCtor)
