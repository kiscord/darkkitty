package kitty.backend.leader

import kotlinx.coroutines.flow.*

interface LeaderElection {
    val status: StateFlow<Status>

    object Single : LeaderElection {
        private val _status = MutableStateFlow(Status.Leader)
        override val status: StateFlow<Status> = _status.asStateFlow()
    }

    enum class Status {
        Leader,
        Follower,
        ;
    }
}
