package kitty.backend.modules.faq

import kitty.backend.*
import kitty.backend.db.*
import kitty.backend.l10n.*
import kiscord.api.*
import kiscord.interactions.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.like
import org.kodein.di.*

class FaqInteractions(di: DI) : AbstractInteractionsController(di) {
    override fun InteractionsHost.Builder.install() {
        with(L10n.Bundle("kitty/backend/l10n/module_faq")) {
            faq()
        }
    }

    context(L10n) private fun InteractionsHost.Builder.faq() {
        command {
            localize("command.faq.name", "command.faq.description")
            dmPermission(false)

            option {
                type(ApplicationCommandOption.Type.String)
                localize("command.faq.params.page.name", "command.faq.params.page.description")
                required(true)
                autocomplete(true)
            }

            option {
                type(ApplicationCommandOption.Type.User)
                localize("command.faq.params.for.name", "command.faq.params.for.description")
                required(false)
            }
        } handle {
            val guildId = interaction.guildId ?: throw IllegalStateException()

            val pageName = data.options?.find { it.name == "page" }?.string
            val forUser = data.options?.find { it.name == "for" }?.snowflake

            val userLocale = interaction.locale ?: interaction.guildLocale ?: Locale.EnglishUS

            if (pageName == null) {
                return@handle message {
                    flag(Message.Flag.Ephemeral)
                    embed {
                        description(get("command.faq.response.no_such_page", userLocale))
                    }
                }
            }

            withTransaction {
                val bookIds = FaqBooks.slice(FaqBooks.id).select { FaqBooks.guildId eq guildId }

                val page = (FaqPages innerJoin FaqPageAliases).select {
                    (FaqPages.book inSubQuery bookIds) and (FaqPageAliases.alias eq pageName)
                }.mapLazy { FaqPage.wrapRow(it) }.limit(1).firstOrNull()
                    ?: (FaqPages innerJoin FaqPageAliases).select {
                        (FaqPages.book inSubQuery bookIds) and (FaqPageAliases.alias like "%${pageName}%")
                    }.mapLazy { FaqPage.wrapRow(it) }.limit(1).firstOrNull()
                if (page != null) {
                    message {
                        content = if (forUser != null) {
                            "<@${forUser}>\n${page.text}".trim()
                        } else {
                            page.text
                        }

                        embed(page.embed)
                    }
                } else {
                    message {
                        flag(Message.Flag.Ephemeral)
                        embed {
                            description(get("command.faq.response.no_such_page", userLocale))
                        }
                    }
                }
            }
        } autocompleteHandle {
            autocomplete {
                val focused = data.options?.find { it.focused }

                val guildId = interaction.guildId ?: throw IllegalStateException()

                when (focused?.name) {
                    "page" -> {
                        withTransaction {
                            val bookIds = FaqBooks.slice(FaqBooks.id).select {
                                FaqBooks.guildId eq guildId
                            }

                            val predicate = mutableListOf<Op<Boolean>>(with(SqlExpressionBuilder) {
                                FaqPages.book inSubQuery bookIds
                            })

                            if (focused.string.isBlank()) {
                                predicate += FaqPageAliases.order eq 0
                            } else {
                                predicate += FaqPageAliases.alias like "%${focused.string}%"
                            }

                            (FaqPages innerJoin FaqPageAliases).slice(
                                FaqPages.embedData,
                                FaqPageAliases.alias
                            ).select {
                                predicate.compoundAnd()
                            }.orderBy(FaqPageAliases.alias, SortOrder.ASC).limit(25).forEach {
                                val alias = it[FaqPageAliases.alias]
                                val embed = EmbedTransformer.toReal(it[FaqPages.embedData])
                                choice("$alias - ${embed.title}", alias)
                            }
                        }
                    }
                }
            }
        }
    }
}