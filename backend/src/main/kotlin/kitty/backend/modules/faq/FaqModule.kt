package kitty.backend.modules.faq

import kitty.backend.*
import kitty.modules.faq.*
import org.flywaydb.core.api.*
import org.jetbrains.exposed.sql.*
import org.kodein.di.*
import org.kodein.di.ktor.controller.*

val FaqModule = DI.Module(name = FaqModuleName.name) {
    inBindSet(tag = "flyway-migrations") { add { singleton { Location("classpath:kitty/backend/modules/faq/db/migrations") } } }

    inBindSet<Table>(tag = "tables-in-use") {
        add { provider { FaqBooks } }
        add { provider { FaqPages } }
        add { provider { FaqPageAliases } }
    }

    inBindSet<InteractionsController> { add { singleton { FaqInteractions(di) } } }
    inBindSet<DIController>(tag = "global-routes") { add { singleton { new(::FaqRoutes) } } }
    inBindSet<ValidationController> { add { singleton { FaqValidator(di) } } }
}
