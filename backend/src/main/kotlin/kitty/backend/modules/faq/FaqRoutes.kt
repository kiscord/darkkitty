package kitty.backend.modules.faq

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.application.Application
import io.ktor.server.auth.*
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.Route
import kiscord.api.*
import kitty.backend.*
import kitty.backend.auth.*
import kitty.backend.routes.*
import kitty.modules.faq.*
import kitty.resources.*

class FaqRoutes(application: Application) : AbstractKittyController(application) {
    override fun Route.getRoutes() {
        authenticate("auth-kitty") {
            get<FaqResource.Books> { resource ->
                val guildId = resource.parent.parent.guildId

                authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                withTransaction {
                    call.respond(FaqBook.findByGuildId(guildId).asDTO())
                }
            }

            post<FaqResource.Books> { resource ->
                val guildId = resource.parent.parent.guildId

                val principal = authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                withTransaction {
                    var book = call.receive<FaqBookDTO>()
                    if (book.author == Snowflake(0UL)) {
                        book = book.copy(author = principal.discordUserId)
                    }
                    val bookModel = FaqBook.new {
                        this.guildId = guildId
                        this.name = book.name
                        this.author = book.author
                    }
                    book.pages.forEach { page ->
                        FaqPage.new {
                            this.book = bookModel
                            this.text = page.text
                            this.embed = page.embed
                        }.also {
                            FaqPage.syncAliases(it, page.aliases)
                        }
                    }
                    call.respond(bookModel.asDTO())
                }
            }

            get<FaqResource.Books.Id> { resource ->
                val guildId = resource.parent.parent.parent.guildId

                authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                withTransaction {
                    call.respond(book(resource).asDTO())
                }
            }

            delete<FaqResource.Books.Id> { resource ->
                val guildId = resource.parent.parent.parent.guildId

                authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                withTransaction {
                    val book = book(resource)
                    book.delete()
                    call.respond(HttpStatusCode.NoContent)
                }
            }

            get<FaqResource.Pages.Id> { resource ->
                val guildId = resource.parent.parent.parent.guildId

                authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                withTransaction {
                    call.respond(page(resource).asDTO())
                }
            }

            put<FaqResource.Pages.Id> { resource ->
                val guildId = resource.parent.parent.parent.guildId

                authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                val body = call.receive<FaqPageDTO>()

                withTransaction {
                    val page = page(resource)
                    page.text = body.text
                    page.embed = body.embed
                    FaqPage.syncAliases(page, body.aliases)
                    call.respond(page.asDTO())
                }
            }

            delete<FaqResource.Pages.Id> { resource ->
                val guildId = resource.parent.parent.parent.guildId

                authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                withTransaction {
                    val page = page(resource)
                    page.delete()
                    call.respond(HttpStatusCode.NoContent)
                }
            }

            post<FaqResource.Books.Id.Pages> { resource ->
                val guildId = resource.parent.parent.parent.parent.guildId

                authorize<KittyPrincipal> { principal ->
                    require { principal.hasGuildManagementPermission(guildId) }
                }

                val body = call.receive<FaqPageDTO>()

                withTransaction {
                    val book = book(resource.parent)
                    val page = FaqPage.new {
                        this.book = book
                        this.text = body.text
                        this.embed = body.embed
                    }
                    FaqPage.syncAliases(page, body.aliases)
                    call.respond(page.asDTO())
                }
            }
        }
    }
}

private fun <T> book(resource: T): FaqBook where T : FaqResource.BookScoped, T : HasParentResource {
    val book = FaqBook.findById(resource.bookId) ?: throw NotFoundException()
    val guildId = resource.of<GuildScoped>().guildId
    if (book.guildId != guildId) throw ForbiddenException()

    return book
}

private fun <T> page(resource: T): FaqPage where T : FaqResource.PageScoped, T : HasParentResource {
    val page = FaqPage.findById(resource.pageId) ?: throw NotFoundException()
    val guildId = resource.of<GuildScoped>().guildId
    if (page.book.guildId != guildId) throw ForbiddenException()

    return page
}

fun Iterable<FaqBook>.asDTO(withId: Boolean = true): List<FaqBookDTO> {
    val pagesCache = FaqPage.find { FaqPages.book inList map { it.id } }.toList()
    val aliasesCache = FaqPageAlias.find { FaqPageAliases.page inList pagesCache.map { it.id } }.toList()

    fun FaqPage.asDTO(): FaqPageDTO {
        val aliases = aliasesCache.filter { it.pageId == id }.map { it.alias }

        return FaqPageDTO(
            id = id.value.takeIf { withId },
            text = text,
            embed = embed,
            aliases = aliases,
        )
    }

    fun FaqBook.asDTO(): FaqBookDTO {
        val pages = pagesCache.filter { it.bookId == id }

        return FaqBookDTO(
            id = id.value.takeIf { withId },
            name = name,
            guildId = guildId,
            author = author,
            pages = pages.map { it.asDTO() },
            createdAt = createdAt.takeIf { withId },
        )
    }

    return map { it.asDTO() }
}
