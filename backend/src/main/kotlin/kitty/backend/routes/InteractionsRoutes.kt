package kitty.backend.routes

import kitty.backend.*
import io.ktor.server.application.*
import io.ktor.server.routing.*
import kiscord.*
import kiscord.interactions.server.*
import org.kodein.di.*
import org.kodein.log.*

class InteractionsRoutes(application: Application) : AbstractKittyController(application) {
    private val loggerFactory: LoggerFactory by instance()
    private val darkKittyConfig: KittyConfig by instance()
    private val kiscord: Kiscord by instance()

    override fun Route.getRoutes() {
        if (darkKittyConfig.mode.isServer) route("/discord-callback") {
            val host = ServerInteractionsHost.builder(loggerFactory).apply {
                this.clientId = darkKittyConfig.clientId
                this.clientSecret = darkKittyConfig.clientSecret

                kitty(di)
            }.build()

            discordInteractionsWebhook(kiscord, host)
        }
    }
}

