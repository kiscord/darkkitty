import org.fusesource.jansi.*

buildscript {
    dependencies {
        classpath(libs.jansi)
    }
}

plugins {
    alias(libs.plugins.properties)
    alias(kiscord.plugins.kotlin.multiplatform) apply false
    alias(kiscord.plugins.kotlin.serialization) apply false
}

allprojects {
    repositories {
        mavenCentral()
        maven("https://gitlab.com/api/v4/projects/12953147/packages/maven") {
            name = "kiscord-gitlab"
            content {
                includeGroup("rocks.aur.kiscord")
            }
        }
        maven("https://maven.pkg.jetbrains.space/public/p/ktor/eap/") {
            name = "ktor-eap"

            content {
                includeGroup("io.ktor")
            }
        }
        maven("https://maven.pkg.jetbrains.space/public/p/kotlinx-coroutines/maven") {
            name = "kotlinx.coroutines"

            content {
                includeGroup("org.jetbrains.kotlinx")
            }
        }
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
        maven("https://oss.sonatype.org/content/repositories/snapshots") {
            name = "sonatype-snapshots"

            mavenContent {
                snapshotsOnly()
            }
        }
        maven("https://s01.oss.sonatype.org/content/repositories/snapshots") {
            name = "sonatype-snapshots-s01"

            mavenContent {
                snapshotsOnly()
            }
        }
        maven("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven") {
            name = "kotlinx.html"

            content {
                includeGroup("org.jetbrains.kotlinx")
            }
        }
    }
}

description = "Kiscord-powered Discord bot"

subprojects {
    version = rootProject.version
}

allprojects {
    if (Ansi.isEnabled()) {
        tasks.withType<JavaExec>().configureEach {
            systemProperties("jansi.mode" to "force")
        }
    }

    tasks.withType<Test>().configureEach {
        reports {
            junitXml.apply {
                required.set(true)
                isOutputPerTestCase = true
            }
        }
    }
}
