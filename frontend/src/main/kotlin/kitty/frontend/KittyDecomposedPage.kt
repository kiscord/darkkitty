package kitty.frontend

import androidx.compose.runtime.*
import kiscord.builder.*
import kitty.frontend.l10n.*
import kitty.frontend.modules.faq.*
import kitty.frontend.modules.pasta.*
import kitty.logic.*
import kitty.modules.pasta.*
import kotlinx.serialization.*
import org.kodein.di.*
import kotlin.reflect.*

data class KittyDecomposedPage<Config : KittyRootComponent.Config, Child : DarkKittyRoot.Child>(
    val configSerializer: KSerializer<Config>,
    val configClass: KClass<Config>,
    val childClass: KClass<Child>,
    val title: @Composable (Config) -> String,
    val renderer: @Composable (Child) -> Unit,
) {
    interface Builder<Config : KittyRootComponent.Config, Child : DarkKittyRoot.Child> :
        BuilderFor<KittyDecomposedPage<Config, Child>> {
        fun configSerializer(serializer: KSerializer<Config>)
        fun configClass(kClass: KClass<Config>)
        fun childClass(kClass: KClass<Child>)

        fun title(block: @Composable (Config) -> String)
        fun renderer(block: @Composable (Child) -> Unit)
    }

    private class BuilderImpl<Config : KittyRootComponent.Config, Child : DarkKittyRoot.Child>
        : Builder<Config, Child> {
        private lateinit var _configSerializer: KSerializer<Config>
        private lateinit var _configClass: KClass<Config>
        private lateinit var _childClass: KClass<Child>
        private var _title: @Composable (Config) -> String = @Composable { "" }
        private var _renderer: @Composable (Child) -> Unit = @Composable {}

        override fun title(block: @Composable (Config) -> String) {
            _title = block
        }

        override fun renderer(block: @Composable (Child) -> Unit) {
            _renderer = block
        }

        override fun configSerializer(serializer: KSerializer<Config>) {
            _configSerializer = serializer
        }

        override fun configClass(kClass: KClass<Config>) {
            _configClass = kClass
        }

        override fun childClass(kClass: KClass<Child>) {
            _childClass = kClass
        }

        override fun build(): KittyDecomposedPage<Config, Child> {
            check(::_configSerializer.isInitialized) { "configSerializer must be initialized" }
            check(::_configClass.isInitialized) { "configClass must be initialized" }
            check(::_childClass.isInitialized) { "childClass must be initialized" }

            return KittyDecomposedPage(
                configSerializer = _configSerializer,
                configClass = _configClass,
                childClass = _childClass,
                title = _title,
                renderer = _renderer,
            )
        }
    }

    companion object {
        fun <Config : KittyRootComponent.Config, Child : DarkKittyRoot.Child> builder(): Builder<Config, Child> {
            return BuilderImpl()
        }
    }
}

fun DI.Builder.bindGlobalEntrypoint(block: KittyEntrypoint.Global.Builder.() -> Unit) {
    inBindSet<KittyEntrypoint> {
        add {
            singleton { KittyEntrypoint.Global(block) }
        }
    }
}

fun DI.Builder.bindGuildEntrypoint(block: KittyEntrypoint.Guild.Builder.() -> Unit) {
    inBindSet<KittyEntrypoint> {
        add {
            singleton { KittyEntrypoint.Guild(block) }
        }
    }
}

fun <Config : KittyRootComponent.Config, Child : DarkKittyRoot.Child> DI.Builder.bindPage(
    page: KittyDecomposedPage<Config, Child>
) {
    inBindSet<KittyDecomposedPage<out KittyRootComponent.Config, out DarkKittyRoot.Child>>(tag = "frontend-pages") {
        add {
            instance(page)
        }
    }
}

inline fun <reified Config : KittyRootComponent.Config, reified Child : DarkKittyRoot.Child> DI.Builder.bindPage(
    block: KittyDecomposedPage.Builder<Config, Child>.() -> Unit
) {
    val page = KittyDecomposedPage.builder<Config, Child>().apply {
        configSerializer(serializer<Config>())
        configClass(Config::class)
        childClass(Child::class)
        block()
    }.build()

    bindPage(page)
}

inline fun <reified Config : KittyRootComponent.Config, reified Child : DarkKittyRoot.Child> DI.Builder.bindNamespacedPage(
    namespace: String,
    block: KittyDecomposedPage.Builder<Config, Child>.() -> Unit
) {
    bindPage {
        NamespacedFrontendPageBuilder(namespace, this).apply(block)
    }
}

@PublishedApi
internal class NamespacedFrontendPageBuilder<Config : KittyRootComponent.Config, Child : DarkKittyRoot.Child>(
    private val namespace: String,
    private val page: KittyDecomposedPage.Builder<Config, Child>
) : KittyDecomposedPage.Builder<Config, Child> by page {
    override fun title(block: @Composable (Config) -> String) {
        page.title { config ->
            SwitchLocalizationContext(namespace = namespace) {
                block(config)
            }
        }
    }

    override fun renderer(block: @Composable (Child) -> Unit) {
        page.renderer { child ->
            SwitchLocalizationContext(namespace = namespace) {
                block(child)
            }
        }
    }
}

@Suppress("UNCHECKED_CAST")
fun <Child : DarkKittyRoot.Child> Iterable<KittyDecomposedPage<*, in Child>>.renderer(child: DarkKittyRoot.Child): (@Composable () -> Unit)? {
    return firstNotNullOfOrNull { page ->
        when {
            page.childClass.isInstance(child) -> {
                @Composable {
                    page.renderer(child as Child)
                }
            }

            else -> null
        }
    }
}

@Composable
@Suppress("UNCHECKED_CAST")
fun <Config : KittyRootComponent.Config> Iterable<KittyDecomposedPage<in Config, *>>.title(config: KittyRootComponent.Config): String? {
    return firstNotNullOfOrNull { page ->
        when {
            page.configClass.isInstance(config) -> page.title(config as Config)
            else -> null
        }
    }
}
