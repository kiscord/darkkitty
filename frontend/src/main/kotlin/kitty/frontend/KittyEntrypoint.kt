package kitty.frontend

import androidx.compose.runtime.*
import kiscord.api.*
import kiscord.builder.*
import kitty.logic.*
import kitty.resources.*

interface KittyEntrypoint {
    val moduleName: ModuleScoped.Name
    val l10nNamespace: String?
    val predicate: @Composable () -> Boolean

    interface GenericBuilder<S : KittyEntrypoint> : BuilderFor<S> {
        fun moduleName(moduleName: ModuleScoped.Name)
        fun l10nNamespace(l10nNamespace: String)
        fun predicate(block: @Composable () -> Boolean)
    }

    interface Global : KittyEntrypoint {
        val config: () -> KittyRootComponent.Config
        val icon: @Composable () -> Unit
        val nested: List<Global>

        interface Builder : GenericBuilder<Global> {
            fun config(block: () -> KittyRootComponent.Config)
            fun icon(block: @Composable () -> Unit)
            fun nested(block: Builder.() -> Unit)
        }

        private data class Impl(
            override val moduleName: ModuleScoped.Name,
            override val l10nNamespace: String?,
            override val predicate: @Composable () -> Boolean,
            override val config: () -> KittyRootComponent.Config,
            override val icon: @Composable () -> Unit,
            override val nested: List<Global>,
        ) : Global

        private class BuilderImpl : Builder {
            private var moduleName: ModuleScoped.Name = ModuleScoped.Name("")
            private var l10nNamespace: String? = null
            private var predicate: @Composable () -> Boolean = @Composable { true }
            private var icon: @Composable () -> Unit = @Composable {}
            private lateinit var config: () -> KittyRootComponent.Config
            private val nested = mutableListOf<() -> Global>()

            override fun moduleName(moduleName: ModuleScoped.Name) {
                this.moduleName = moduleName
            }

            override fun l10nNamespace(l10nNamespace: String) {
                this.l10nNamespace = l10nNamespace
            }

            override fun predicate(block: @Composable () -> Boolean) {
                this.predicate = block
            }

            override fun icon(block: @Composable () -> Unit) {
                this.icon = block
            }

            override fun config(block: () -> KittyRootComponent.Config) {
                this.config = block
            }

            override fun nested(block: Builder.() -> Unit) {
                nested += {
                    builder {
                        moduleName(this@BuilderImpl.moduleName)
                        this@BuilderImpl.l10nNamespace?.let { l10nNamespace(it) }
                        block()
                    }.build()
                }
            }

            override fun build(): Global {
                check(moduleName.name.isNotBlank())
                return Impl(
                    moduleName = moduleName,
                    l10nNamespace = l10nNamespace,
                    predicate = predicate,
                    icon = icon,
                    config = config,
                    nested = nested.map { it() }
                )
            }
        }

        companion object : BuilderSpec<Global, Builder> {
            override fun builder(): Builder = BuilderImpl()
        }
    }

    interface Guild : KittyEntrypoint {
        val config: (guildId: Snowflake) -> KittyRootComponent.Config

        interface Builder : GenericBuilder<Guild> {
            fun config(block: (guildId: Snowflake) -> KittyRootComponent.Config)
        }

        private data class Impl(
            override val moduleName: ModuleScoped.Name,
            override val l10nNamespace: String?,
            override val predicate: @Composable () -> Boolean,
            override val config: (guildId: Snowflake) -> KittyRootComponent.Config,
        ) : Guild

        private class BuilderImpl : Builder {
            private var moduleName: ModuleScoped.Name = ModuleScoped.Name("")
            private var l10nNamespace: String? = null
            private var predicate: @Composable () -> Boolean = @Composable { true }
            private lateinit var config: (guildId: Snowflake) -> KittyRootComponent.Config

            override fun moduleName(moduleName: ModuleScoped.Name) {
                this.moduleName = moduleName
            }

            override fun l10nNamespace(l10nNamespace: String) {
                this.l10nNamespace = l10nNamespace
            }

            override fun predicate(block: @Composable () -> Boolean) {
                this.predicate = block
            }

            override fun config(block: (guildId: Snowflake) -> KittyRootComponent.Config) {
                this.config = block
            }

            override fun build(): Guild {
                check(moduleName.name.isNotBlank())
                return Impl(
                    moduleName = moduleName,
                    l10nNamespace = l10nNamespace,
                    predicate = predicate,
                    config = config,
                )
            }
        }

        companion object : BuilderSpec<Guild, Builder> {
            override fun builder(): Builder = BuilderImpl()
        }
    }
}
