@file:JsModule("jose")
@file:JsQualifier("errors")

package kitty.frontend.auth

import kitty.frontend.js.*


open external class JOSEError(message: String = definedExternally) : JsError {
    companion object {
        fun code(): String
    }
}

open external class JWTClaimValidationFailed(
    message: String,
    claim: String = definedExternally,
    reason: String = definedExternally
) : JOSEError {
    companion object {
        fun code(): String
    }
}

open external class JWTExpired(
    message: String,
    claim: String = definedExternally,
    reason: String = definedExternally
) : JWTClaimValidationFailed {
    companion object {
        fun code(): String
    }
}