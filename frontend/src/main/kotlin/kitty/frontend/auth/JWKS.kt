@file:JsModule("jose")

package kitty.frontend.auth

import org.w3c.dom.url.*

external fun createLocalJWKSet(jwks: JSONWebKeySet): GetKeyFunction<JWSHeaderParameters, FlattenedJWSInput>

external fun createRemoteJWKSet(
    url: URL,
    options: RemoteJWKSetOptions = definedExternally
): GetKeyFunction<JWSHeaderParameters, FlattenedJWSInput>