@file:JsModule("jose")

package kitty.frontend.auth

import org.khronos.webgl.*
import kotlin.js.Promise

external fun jwtVerify(
    jwt: String,
    key: KeyLike,
    options: JWTVerifyOptions = definedExternally
): Promise<JWTVerifyResult>

external fun jwtVerify(jwt: String, key: KeyLike): Promise<JWTVerifyResult>

external fun jwtVerify(
    jwt: String,
    key: Uint8Array,
    options: JWTVerifyOptions = definedExternally
): Promise<JWTVerifyResult>

external fun jwtVerify(jwt: String, key: Uint8Array): Promise<JWTVerifyResult>

external fun jwtVerify(
    jwt: Uint8Array,
    key: KeyLike,
    options: JWTVerifyOptions = definedExternally
): Promise<JWTVerifyResult>

external fun jwtVerify(jwt: Uint8Array, key: KeyLike): Promise<JWTVerifyResult>

external fun jwtVerify(
    jwt: Uint8Array,
    key: Uint8Array,
    options: JWTVerifyOptions = definedExternally
): Promise<JWTVerifyResult>

external fun jwtVerify(jwt: Uint8Array, key: Uint8Array): Promise<JWTVerifyResult>

external fun jwtVerify(
    jwt: String,
    getKey: GetKeyFunction<JWTHeaderParameters, FlattenedJWSInput>,
    options: JWTVerifyOptions = definedExternally
): Promise<JWTVerifyResultWithResolvedKey>

external fun jwtVerify(
    jwt: String,
    getKey: GetKeyFunction<JWTHeaderParameters, FlattenedJWSInput>
): Promise<JWTVerifyResultWithResolvedKey>

external fun jwtVerify(
    jwt: Uint8Array,
    getKey: GetKeyFunction<JWTHeaderParameters, FlattenedJWSInput>,
    options: JWTVerifyOptions = definedExternally
): Promise<JWTVerifyResultWithResolvedKey>

external fun jwtVerify(
    jwt: Uint8Array,
    getKey: GetKeyFunction<JWTHeaderParameters, FlattenedJWSInput>
): Promise<JWTVerifyResultWithResolvedKey>
