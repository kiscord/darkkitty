package kitty.frontend.auth

import kitty.frontend.js.*
import kotlinx.coroutines.*
import org.khronos.webgl.*

typealias GetKeyFunction<T, T2> = JsFunction2<T, T2, dynamic /* Promise<dynamic /* KeyLike | Uint8Array */> | KeyLike | Uint8Array */>

inline operator fun JWK.get(propName: String): Any? = asDynamic()[propName]

inline operator fun JWK.set(propName: String, value: Any) {
    asDynamic()[propName] = value
}

inline operator fun JWSHeaderParameters.get(propName: String): Any? = asDynamic()[propName]

inline operator fun JWSHeaderParameters.set(propName: String, value: Any) {
    asDynamic()[propName] = value
}

inline operator fun Crit.get(propName: String): Boolean? = asDynamic()[propName] as? Boolean

inline operator fun Crit.set(propName: String, value: Boolean) {
    asDynamic()[propName] = value
}

inline operator fun JWTPayload.get(propName: String): Any? = asDynamic()[propName]

inline operator fun JWTPayload.set(propName: String, value: Any) {
    asDynamic()[propName] = value
}

@PublishedApi
internal inline fun jwtVerifyOptions(block: JWTVerifyOptions.() -> Unit): JWTVerifyOptions {
    val options = js("{}").unsafeCast<JWTVerifyOptions>()
    options.apply(block)
    return options
}

suspend inline fun jwtVerifySync(
    jwt: String,
    key: KeyLike,
    options: JWTVerifyOptions.() -> Unit
): JWTVerifyResult = jwtVerify(jwt, key, jwtVerifyOptions(options)).await()


suspend inline fun jwtVerifySync(
    jwt: String,
    key: Uint8Array,
    options: JWTVerifyOptions.() -> Unit
): JWTVerifyResult = jwtVerify(jwt, key, jwtVerifyOptions(options)).await()

suspend inline fun jwtVerifySync(
    jwt: Uint8Array,
    key: KeyLike,
    options: JWTVerifyOptions.() -> Unit
): JWTVerifyResult = jwtVerify(jwt, key, jwtVerifyOptions(options)).await()

suspend inline fun jwtVerifySync(
    jwt: Uint8Array,
    key: Uint8Array,
    options: JWTVerifyOptions.() -> Unit
): JWTVerifyResult = jwtVerify(jwt, key, jwtVerifyOptions(options)).await()

suspend inline fun jwtVerifySync(
    jwt: String,
    getKey: GetKeyFunction<JWTHeaderParameters, FlattenedJWSInput>,
    options: JWTVerifyOptions.() -> Unit
): JWTVerifyResultWithResolvedKey = jwtVerify(jwt, getKey, jwtVerifyOptions(options)).await()

suspend inline fun jwtVerifySync(
    jwt: Uint8Array,
    getKey: GetKeyFunction<JWTHeaderParameters, FlattenedJWSInput>,
    options: JWTVerifyOptions.() -> Unit
): JWTVerifyResultWithResolvedKey = jwtVerify(jwt, getKey, jwtVerifyOptions(options)).await()
