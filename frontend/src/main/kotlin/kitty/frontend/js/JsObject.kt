package kitty.frontend.js

inline fun <T> jso(builder: dynamic.() -> Unit): T {
    val obj = js("{}")
    builder(obj)
    return obj.unsafeCast<T>()
}

