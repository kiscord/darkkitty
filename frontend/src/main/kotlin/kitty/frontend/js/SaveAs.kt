package kitty.frontend.js

import kotlinx.browser.*
import org.w3c.dom.*
import org.w3c.dom.url.*
import org.w3c.files.*

fun saveAs(blob: Blob, filename: String) {
    val a = document.createElement("a") as HTMLAnchorElement
    a.href = URL.createObjectURL(blob)
    a.download = filename
    a.click()
    URL.revokeObjectURL(a.href)
}