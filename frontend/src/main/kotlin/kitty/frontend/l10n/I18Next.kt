@file:JsModule("i18next")

package kitty.frontend.l10n

import kitty.frontend.js.*
import org.jetbrains.compose.web.css.*

external interface I18Next {
    val language: LanguageCode?
    val languages: Array<LanguageCode>?
    val resolvedLanguage: LanguageCode?

    fun init(initOptions: InitOptions = definedExternally, callback: (Error?, dynamic) -> Unit = definedExternally)
    fun use(module: dynamic): I18Next

    fun t(keys: String, options: dynamic = definedExternally): String
    fun t(keys: Array<String>, options: dynamic = definedExternally): String

    fun on(event: String, listener: (dynamic) -> Unit)
    fun off(event: String, listener: (dynamic) -> Unit = definedExternally)

    fun changeLanguage(language: LanguageCode)

    fun getFixedT(
        language: LanguageCode? = definedExternally,
        namespace: String? = definedExternally,
        prefix: String? = definedExternally,
    ): JsFunction2<String, dynamic, String>

    fun loadNamespaces(ns: String)
    fun loadNamespaces(ns: Array<String>)
}

external interface InitOptions {
    @JsName("lng")
    var language: LanguageCode?
        get() = definedExternally
        set(value) = definedExternally

    @JsName("fallbackLng")
    var fallbackLanguage: LanguageCode?
        get() = definedExternally
        set(value) = definedExternally
    var debug: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var resources: dynamic
        get() = definedExternally
        set(value) = definedExternally
    var backend: dynamic
        get() = definedExternally
        set(value) = definedExternally
}

external fun createInstance(
    initOptions: InitOptions = definedExternally,
    callback: (Error?, dynamic) -> Unit = definedExternally
): I18Next
