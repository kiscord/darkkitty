package kitty.frontend.modules.faq

import com.arkivanov.essenty.parcelable.*
import io.ktor.resources.*
import kiscord.api.*
import kitty.logic.*
import kitty.resources.*
import kotlinx.serialization.*
import org.kodein.di.*

@Parcelize
@Serializable
@Resource("/faq")
data class FaqConfig(
    override val parent: GuildOverview,
) : KittyRootComponent.Config(), GuildScoped {
    constructor(guildId: Snowflake) : this(parent = GuildOverview(guildId = guildId))

    override val guildId: Snowflake get() = parent.guildId

    override fun createChild(di: DI) = FaqGuildOverview(di, parent.guildId)

    @Parcelize
    @Serializable
    @Resource("/page/{pageId}")
    data class PageEdit(
        override val parent: FaqConfig,
        val pageId: Long,
    ) : KittyRootComponent.Config(), GuildScoped {
        constructor(guildId: Snowflake, pageId: Long) : this(parent = FaqConfig(guildId = guildId), pageId = pageId)

        override val guildId: Snowflake get() = parent.guildId

        override fun createChild(di: DI) = FaqEditPageChild(di, guildId, pageId)
    }

    @Parcelize
    @Serializable
    @Resource("/books/{bookId}/create")
    data class PageCreate(
        override val parent: FaqConfig,
        val bookId: Long,
    ) : KittyRootComponent.Config(), GuildScoped {
        constructor(guildId: Snowflake, bookId: Long) : this(parent = FaqConfig(guildId = guildId), bookId = bookId)

        override val guildId: Snowflake get() = parent.guildId

        override fun createChild(di: DI) = FaqCreatePageChild(di, guildId, bookId)
    }
}
