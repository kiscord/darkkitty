package kitty.frontend.modules.faq

import kitty.frontend.*
import kitty.frontend.js.*
import kitty.frontend.l10n.*
import kitty.modules.faq.*
import org.kodein.di.*

private const val L10N_NAMESPACE = "module_faq"

val FaqModule = DI.Module(FaqModuleName.name) {
    require("@/css/faq.scss")

    bindGuildEntrypoint {
        moduleName(FaqModuleName)
        l10nNamespace(L10N_NAMESPACE)
        config { guildId ->
            FaqConfig(guildId = guildId)
        }
    }

    bindSingleton<FaqService> { FaqService.Default(instance()) }

    bindNamespacedPage<FaqConfig, FaqGuildOverview>(L10N_NAMESPACE) {
        title { l10n("navigation.faq") }
        renderer { child ->
            FaqGuildOverviewUi(child)
        }
    }

    bindNamespacedPage<FaqConfig.PageEdit, FaqEditPageChild>(L10N_NAMESPACE) {
        title { l10n("navigation.faq.edit_page") }
        renderer { child ->
            FaqEditPageUi(child)
        }
    }

    bindNamespacedPage<FaqConfig.PageCreate, FaqCreatePageChild>(L10N_NAMESPACE) {
        title { l10n("navigation.faq.create_page") }
        renderer { child ->
            FaqCreatePageUi(child)
        }
    }
}
