package kitty.frontend.modules.faq

import kitty.logic.*
import kitty.modules.faq.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kiscord.api.*

interface FaqService {
    fun books(guildId: Snowflake): FlushableStateFlow<List<FaqBookDTO>?>
    fun page(guildId: Snowflake, pageId: Long): FlushableStateFlow<FaqPageDTO?>
    suspend fun createBook(guildId: Snowflake, book: FaqBookDTO): FaqBookDTO
    suspend fun createPage(guildId: Snowflake, bookId: Long, page: FaqPageDTO): FaqPageDTO
    suspend fun deleteBook(guildId: Snowflake, bookId: Long): Boolean
    suspend fun deletePage(guildId: Snowflake, pageId: Long): Boolean
    suspend fun savePage(guildId: Snowflake, pageId: Long, page: FaqPageDTO): FaqPageDTO

    class Default(service: ServiceIO) : ServiceIO by service, FaqService {
        override fun books(guildId: Snowflake): FlushableStateFlow<List<FaqBookDTO>?> =
            resource(FaqResource.Books(guildId = guildId)) {
                method = HttpMethod.Get
            }

        override fun page(guildId: Snowflake, pageId: Long): FlushableStateFlow<FaqPageDTO?> =
            resource(FaqResource.Pages.Id(guildId = guildId, pageId = pageId)) {
                method = HttpMethod.Get
            }

        override suspend fun deleteBook(guildId: Snowflake, bookId: Long): Boolean {
            val response: HttpResponse? = oneshotResource(FaqResource.Books.Id(guildId = guildId, bookId = bookId)) {
                method = HttpMethod.Delete
            }
            return response?.status == HttpStatusCode.NoContent
        }

        override suspend fun savePage(guildId: Snowflake, pageId: Long, page: FaqPageDTO): FaqPageDTO {
            return oneshotResource(FaqResource.Pages.Id(guildId = guildId, pageId = pageId)) {
                method = HttpMethod.Put
                contentType(defaultRequestContentType)
                setBody(page)
            } ?: throw IllegalStateException("Unable to save page")
        }

        override suspend fun createPage(guildId: Snowflake, bookId: Long, page: FaqPageDTO): FaqPageDTO {
            return oneshotResource(FaqResource.Books.Id.Pages(guildId = guildId, bookId = bookId)) {
                method = HttpMethod.Post
                contentType(defaultRequestContentType)
                setBody(page)
            } ?: throw IllegalStateException("Unable to create page")
        }

        override suspend fun deletePage(guildId: Snowflake, pageId: Long): Boolean {
            val response: HttpResponse? = oneshotResource(FaqResource.Pages.Id(guildId = guildId, pageId = pageId)) {
                method = HttpMethod.Delete
            }
            return response?.status == HttpStatusCode.NoContent
        }

        override suspend fun createBook(guildId: Snowflake, book: FaqBookDTO): FaqBookDTO {
            return oneshotResource(FaqResource.Books(guildId = guildId)) {
                method = HttpMethod.Post
                contentType(defaultRequestContentType)
                setBody(book)
            } ?: throw IllegalStateException("Unable to create book")
        }
    }
}