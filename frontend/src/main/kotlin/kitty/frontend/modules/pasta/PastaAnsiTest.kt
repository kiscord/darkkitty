package kitty.frontend.modules.pasta

import androidx.compose.runtime.*
import kitty.frontend.js.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.frontend.ui.rich_text.*
import kitty.logic.*
import kitty.rich_text.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.*

class PastaAnsiTest(di: DI) : DarkKittyRoot.Child(di) {
    val lines: List<RichText> by lazy {
        buildList {
            with(RichText) {
                add(text("4 bit palette"))
                for (bgRaw in 0..15) {
                    val bg = RichTextColor.Palette(bgRaw.toUByte())
                    var line = empty
                    for (fgRaw in 0..15) {
                        val fg = RichTextColor.Palette(fgRaw.toUByte())
                        line += foregroundColor(
                            fg,
                            text(fgRaw.toString().padStart(2, '0')),
                        )
                    }
                    add(backgroundColor(bg, line))
                }
                add(text("6x6x6 color cube"))
                for (msb in 0..5) {
                    var upperLine = empty
                    for (lsb in 0..17) {
                        val index = msb * 36 + lsb + 16
                        val color = RichTextColor.Palette(index.toUByte())
                        upperLine += backgroundColor(
                            color,
                            text(index.toString().padStart(3, '0')),
                        )
                    }
                    var lowerLine = empty

                    for (lsb in 18..35) {
                        val index = msb * 36 + lsb + 16
                        val color = RichTextColor.Palette(index.toUByte())
                        lowerLine += backgroundColor(
                            color,
                            text(index.toString().padStart(3, '0')),
                        )
                    }
                    add(
                        foregroundColor(RichTextColor.Palette(15.toUByte()), upperLine)
                                + foregroundColor(RichTextColor.Palette(0.toUByte()), lowerLine)
                    )
                }
                add(text("rgb colors"))
                val yellow = RichTextColor.RGB(0xefa94a.toUInt())
                val blue = RichTextColor.RGB(0x5f6fa3.toUInt())
                add(backgroundColor(yellow, foregroundColor(blue, text("blue over yellow"))))
                add(backgroundColor(blue, foregroundColor(yellow, text("yellow over blue"))))

                add(text("text decorations"))
                add(faint(text("Faint")))
                add(bold(text("Bold")))
                add(italic(text("Italic")))
                add(faint(italic(text("Faint + Italic"))))
                add(bold(italic(text("Bold + Italic"))))
                add(
                    underline(
                        text("Underline ") + underlineColor(
                            RichTextColor.Palette(1.toUByte()),
                            text("(with custom colors too!)")
                        )
                    )
                )
            }
        }
    }
}

@Composable
fun PastaAnsiTestUi(content: PastaAnsiTest) {
    require("@/css/pasta.scss")

    Section(attrs = {
        classes("section", "is-main-section", "is-borderless")
    }) {
        val pastaLogViewerController = remember { SharedPastaViewerController() }
        pastaLogViewerController.observe()

        val settings = movableContentOf {
            PastaViewerSettings(pastaLogViewerController)
        }

        ProvidePastaLogViewerController(pastaLogViewerController) {
            pastaLogViewerController.renderer.render(content.lines) {
                settings()
            }
        }
    }
}
