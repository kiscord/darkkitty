package kitty.frontend.modules.pasta

import androidx.compose.runtime.*
import kitty.frontend.ui.rich_text.*
import kitty.frontend.ui.blocks.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*


@Composable
fun PastaComposeRenderer(lines: List<RichText>, container: ContentBuilder<HTMLDivElement>? = null) {
    val pastaViewerController = PastaViewerController.current

    LazyListFixedHeight(lines, 100.percent, attrs = {
        classes("pasta", "ansi")
        pastaViewerController.palette.cssClassName?.let { classes(it) }
        style {
            variable("--max-line-number-width", 1.ch * lines.size.toString().length)
            fontSize(pastaViewerController.fontSizeInPt.pt)
        }
    }, itemAttrs = { _, index ->
        id("L$index")
        if (index % 2 == 0) {
            classes("odd")
        } else {
            classes("even")
        }
    }, container = container) { item, index ->
        Span(attrs = {
            classes("line-number")
        }) { Text((index + 1).toString()) }
        Code { item.renderComposable() }
    }

}
