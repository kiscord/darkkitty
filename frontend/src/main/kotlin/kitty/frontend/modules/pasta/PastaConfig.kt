package kitty.frontend.modules.pasta

import com.arkivanov.essenty.parcelable.*
import io.ktor.resources.*
import kiscord.api.*
import kitty.*
import kitty.logic.*
import kitty.modules.pasta.*
import kitty.resources.*
import kotlinx.serialization.*
import org.kodein.di.*

@Parcelize
@Serializable
@Resource("/pasta")
object PastaConfig : KittyRootComponent.Config() {
    override fun createChild(di: DI): DarkKittyRoot.Child = PastaOverview(di)

    @Parcelize
    @Serializable
    @Resource("/{id}")
    class View(
        override val parent: PastaConfig = PastaConfig,
        val id: HashId,
    ) : KittyRootComponent.Config(), HasParentResource {
        constructor(pastaFile: PastaFileDTO) : this(id = pastaFile.id) {
            cachedPastaFile = pastaFile
        }

        @Transient
        private var cachedPastaFile: PastaFileDTO? = null

        override fun createChild(di: DI): DarkKittyRoot.Child {
            return cachedPastaFile?.let { PastaView(di, it) } ?: PastaView(di, id)
        }
    }

    @Parcelize
    @Serializable
    @Resource("/ansi_test")
    data class AnsiTest(
        override val parent: PastaConfig = PastaConfig,
    ) : KittyRootComponent.Config(), HasParentResource {
        override fun createChild(di: DI): DarkKittyRoot.Child = PastaAnsiTest(di)
    }
}
