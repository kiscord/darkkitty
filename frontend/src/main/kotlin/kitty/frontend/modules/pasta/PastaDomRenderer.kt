package kitty.frontend.modules.pasta

import androidx.compose.runtime.*
import kitty.frontend.ui.rich_text.*
import kotlinx.browser.*
import kotlinx.dom.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun PastaDomRenderer(lines: List<RichText>, container: ContentBuilder<HTMLDivElement>? = null) {
    val pastaViewerController = PastaViewerController.current

    Div(attrs = {
        classes("pasta", "ansi", "is-flexible")
        pastaViewerController.palette.cssClassName?.let { classes(it) }
        style {
            variable("--max-line-number-width", 1.ch * lines.size.toString().length)
            fontSize(pastaViewerController.fontSizeInPt.pt)
        }
    }) {
        container?.invoke(this)

        DisposableEffect(lines) {
            lines.forEachIndexed { index, line ->
                val lineDom = document.createElement("div") as HTMLDivElement
                lineDom.id = "L$index"
                if (index % 2 == 0) {
                    lineDom.className = "list-item odd"
                } else {
                    lineDom.className = "list-item even"
                }

                val lineNumberDom = document.createElement("span") as HTMLSpanElement
                lineNumberDom.className = "line-number"
                lineNumberDom.textContent = (index + 1).toString()
                lineDom.appendChild(lineNumberDom)

                val lineCodeDom = document.createElement("code") as HTMLElement
                line.renderToDOM(lineCodeDom)
                lineDom.appendChild(lineCodeDom)

                scopeElement.appendChild(lineDom)
            }

            onDispose {
                val list = scopeElement.querySelectorAll("div.list-item")
                for (index in 0 until list.length) {
                    val item = list.item(index) ?: continue
                    scopeElement.removeChild(item)
                }
            }
        }
    }
}
