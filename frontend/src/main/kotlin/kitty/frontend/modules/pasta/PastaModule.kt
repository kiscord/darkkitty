package kitty.frontend.modules.pasta

import kitty.frontend.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.modules.pasta.*
import org.kodein.di.*

private const val L10N_NAMESPACE = "module_pasta"

val PastaModule = DI.Module(PastaModuleName.name) {
    bindGlobalEntrypoint {
        moduleName(PastaModuleName)
        l10nNamespace(L10N_NAMESPACE)
        config { PastaConfig }
        icon { MDIIcon("pasta") }
    }

    bindSingleton<PastaService> { PastaService.Default(instance()) }

    bindNamespacedPage<PastaConfig, PastaOverview>(L10N_NAMESPACE) {
        title { l10n("navigation.pasta") }
        renderer { child ->
            PastaOverviewUi(child)
        }
    }

    bindNamespacedPage<PastaConfig.View, PastaView>(L10N_NAMESPACE) {
        title { config ->
            l10n("navigation.pasta.view") {
                pasta_id = config.id
            }
        }
        renderer { child ->
            PastaViewUi(child)
        }
    }

    bindNamespacedPage<PastaConfig.AnsiTest, PastaAnsiTest>(L10N_NAMESPACE) {
        title { l10n("navigation.pasta.ansi_test") }
        renderer { child ->
            PastaAnsiTestUi(child)
        }
    }
}
