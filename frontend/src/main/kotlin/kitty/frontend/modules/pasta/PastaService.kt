package kitty.frontend.modules.pasta

import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.*
import kitty.*
import kitty.logic.*
import kitty.modules.pasta.*
import org.w3c.files.*

interface PastaService {
    suspend fun uploadFile(file: File): PastaFileDTO
    suspend fun uploadText(text: String): PastaFileDTO

    suspend fun get(id: HashId): PastaFileDTO
    suspend fun raw(id: HashId): ByteReadChannel

    class Default(service: ServiceIO) : ServiceIO by service, PastaService {
        override suspend fun uploadFile(file: File): PastaFileDTO {
            val fileData = file.readByteArray()
            return httpClient.post(PastaResource.Upload()) {
                setBody(MultiPartFormDataContent(
                    formData {
                        append("contents", fileData, Headers.build {
                            append(HttpHeaders.ContentType, file.type)
                            append(
                                HttpHeaders.ContentDisposition,
                                "${ContentDisposition.Parameters.FileName}=${file.name.escapeIfNeeded()}"
                            )
                        })
                    }
                ))
            }.body<PastaFileDTO>()
        }

        override suspend fun uploadText(text: String): PastaFileDTO {
            return httpClient.post(PastaResource.Upload()) {
                setBody(MultiPartFormDataContent(
                    formData {
                        append("contents", text, Headers.build {
                            contentType(ContentType.Text.Plain)
                        })
                    }
                ))
            }.body<PastaFileDTO>()
        }

        override suspend fun get(id: HashId): PastaFileDTO {
            return httpClient.get(PastaResource.Id(id = id)).body()
        }

        override suspend fun raw(id: HashId): ByteReadChannel {
            return httpClient.get(PastaResource.Id.Raw(id = id)).bodyAsChannel()
        }
    }
}
