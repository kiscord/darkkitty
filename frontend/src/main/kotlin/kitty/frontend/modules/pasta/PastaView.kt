package kitty.frontend.modules.pasta

import androidx.compose.runtime.*
import kitty.*
import kitty.rich_text.*
import kitty.frontend.js.*
import kitty.frontend.ui.rich_text.*
import kitty.logic.*
import kitty.modules.pasta.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.*

class PastaView : DarkKittyRoot.Child {
    val pastaFile: Deferred<PastaFileDTO>
    val id: HashId

    val scope = coroutineScope(Dispatchers.Default)

    constructor(di: DI, pastaFile: PastaFileDTO) : super(di) {
        this.id = pastaFile.id
        this.pastaFile = CompletableDeferred(pastaFile)
    }

    constructor(di: DI, id: HashId) : super(di) {
        this.id = id
        this.pastaFile = coroutineScope().async {
            service.get(id)
        }
    }

    private val service: PastaService by instance()

    fun <T : MutableCollection<RichText>> loadPasta(lines: T, block: suspend (T) -> Unit) {
        scope.launch {
            val pastaFile = pastaFile.await()
            val raw = service.raw(pastaFile.id)
            RichTextSequencer(RichText).parse(raw).toCollection(lines)
            block(lines)
        }
    }
}

@Composable
fun PastaViewUi(context: PastaView) {
    require("@/css/pasta.scss")

    var lines by remember { mutableStateOf(emptyList<RichText>()) }

    remember {
        context.loadPasta(mutableListOf()) {
            lines = it
        }
    }

    val pastaLogViewerController = remember { SharedPastaViewerController() }
    pastaLogViewerController.observe()

    val settings = movableContentOf {
        PastaViewerSettings(pastaLogViewerController)
    }

    Section(attrs = {
        classes("section", "is-main-section", "is-borderless")
    }) {
        ProvidePastaLogViewerController(pastaLogViewerController) {
            pastaLogViewerController.render(lines) {
                settings()
            }
        }
    }
}
