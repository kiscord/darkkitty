package kitty.frontend.modules.pasta

import androidx.compose.runtime.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.rich_text.*
import kitty.logic.*
import kotlinx.coroutines.flow.*
import kotlinx.serialization.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Stable
interface PastaViewerController {
    var palette: RichTextColorPalette
    var renderer: Renderer
    var fontSizeInPt: Int

    enum class Renderer {
        Compose {
            override val displayName: String
                @Composable
                @ReadOnlyComposable
                get() = l10n.forContext(namespace = "module_pasta")("renderer.compose")

            @Composable
            override fun render(lines: List<RichText>, container: ContentBuilder<HTMLDivElement>?) {
                PastaComposeRenderer(lines, container)
            }
        },
        DOM {
            override val displayName: String
                @Composable
                @ReadOnlyComposable
                get() = l10n.forContext(namespace = "module_pasta")("renderer.dom")

            @Composable
            override fun render(lines: List<RichText>, container: ContentBuilder<HTMLDivElement>?) {
                PastaDomRenderer(lines, container)
            }
        },
        ;

        abstract val displayName: String
            @Composable
            @ReadOnlyComposable
            get

        @Composable
        abstract fun render(lines: List<RichText>, container: ContentBuilder<HTMLDivElement>?)
    }

    companion object {
        val current: PastaViewerController
            @Composable
            @ReadOnlyComposable
            get() = LocalPastaViewerController.current
    }
}

@Composable
@NonRestartableComposable
fun PastaViewerController.render(lines: List<RichText>, container: ContentBuilder<HTMLDivElement>? = null) {
    renderer.render(lines, container)
}

private val LocalPastaViewerController = staticCompositionLocalOf<PastaViewerController> {
    throw IllegalStateException("No PastaLogViewerController in current context")
}

@Composable
fun ProvidePastaLogViewerController(controller: PastaViewerController, content: @Composable () -> Unit) {
    CompositionLocalProvider(LocalPastaViewerController provides controller, content = content)
}

@Stable
class SharedPastaViewerController(key: String = "pasta-log-viewer") : PastaViewerController {
    private val storage = SerializedStorage(key, State.serializer())
    private var state by mutableStateOf(State.Default)

    private val _palette = derivedStateOf { state.palette }
    override var palette: RichTextColorPalette
        get() = _palette.value
        set(value) {
            storage.set(state.copy(palette = value))
        }

    private val _renderer = derivedStateOf { state.renderer }
    override var renderer: PastaViewerController.Renderer
        get() = _renderer.value
        set(value) {
            storage.set(state.copy(renderer = value))
        }

    private val _fontSize = derivedStateOf { state.fontSizeInPt }
    override var fontSizeInPt: Int
        get() = _fontSize.value
        set(value) {
            storage.set(state.copy(fontSizeInPt = value))
        }

    @Composable
    fun observe() {
        LaunchedEffect(storage.flow) {
            storage.flow.collect {
                state = it ?: State.Default
            }
        }
    }

    @Serializable
    @OptIn(ExperimentalSerializationApi::class)
    private data class State(
        @EncodeDefault(EncodeDefault.Mode.NEVER)
        val palette: RichTextColorPalette = RichTextColorPalette.BrowserDefault,
        @EncodeDefault(EncodeDefault.Mode.NEVER)
        val renderer: PastaViewerController.Renderer = PastaViewerController.Renderer.Compose,
        @EncodeDefault(EncodeDefault.Mode.NEVER)
        val fontSizeInPt: Int = 14,
    ) {
        companion object {
            val Default = State()
        }
    }
}
