package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*

@Composable
fun Field(
    label: String? = null,
    hasAddons: Boolean = false,
    isGrouped: Boolean = false,
    attrs: AttrBuilderContext<HTMLDivElement>? = null,
    content: ContentBuilder<HTMLDivElement>
) {
    Div(attrs = {
        classes("field")
        if (hasAddons) classes("has-addons")
        if (isGrouped) classes("is-grouped")
        attrs?.invoke(this)
    }) {
        if (label != null) Label(attrs = {
            classes("label")
        }) { Text(label) }
        content()
    }
}

@Composable
fun HorizontalField(label: String = "", content: ContentBuilder<HTMLDivElement>) {
    Div(attrs = {
        classes("field", "is-horizontal")
    }) {
        Div(attrs = {
            classes("field-label", "is-normal")
        }) {
            Label(attrs = {
                classes("label")
            }) { Text(label) }
        }
        Div(attrs = {
            classes("field-body")
        }) {
            content()
        }
    }
}

@Composable
fun HorizontalFieldWithSwitch(
    active: Boolean,
    onToggle: (Boolean) -> Unit,
    label: String,
    content: ContentBuilder<HTMLDivElement>
) {
    Div(attrs = {
        classes("field", "is-horizontal")
    }) {
        Div(attrs = {
            classes("field-label", "is-normal", "has-switch")
        }) {
            Switch(active, onToggle, label, attrs = {
                classes("label")
            })
        }
        Div(attrs = {
            classes("field-body")
        }) {
            content()
        }
    }
}

@Composable
fun Control(
    isExpanded: Boolean = false,
    icon: ContentBuilder<HTMLDivElement>? = null,
    rightIcon: ContentBuilder<HTMLDivElement>? = null,
    attrs: AttrBuilderContext<HTMLDivElement>? = null,
    content: ContentBuilder<HTMLDivElement>
) {
    Div(attrs = {
        classes("control")
        if (isExpanded) classes("is-expanded")
        if (icon != null) classes("has-icons-left")
        if (rightIcon != null) classes("has-icons-right")
        attrs?.invoke(this)
    }) {
        content()
        if (icon != null) ProvideIconAlignment(IconAlignment.Left) {
            icon.invoke(this)
        }
        if (rightIcon != null) ProvideIconAlignment(IconAlignment.Right) {
            rightIcon.invoke(this)
        }
    }
}