package kitty.frontend.ui.blocks

import androidx.compose.runtime.*

@Composable
fun <T : Any> Grid(items: Collection<T>, size: GridSize = GridSize.Medium, item: @Composable (T) -> Unit) {
    Tile(isAncestor = true, isVertical = true) {
        items.chunked(size.itemsPerRow).forEach { row ->
            Tile {
                row.forEach {
                    Tile(isParent = true, attrs = { classes("is-${size.itemSize}") }) {
                        item(it)
                    }
                }
            }
        }
    }
}

enum class GridSize(internal val itemSize: Int, internal val itemsPerRow: Int) {
    ExtraLarge(12, 1),
    Large(6, 2),
    Big(4, 3),
    Medium(3, 4),
    Small(2, 6),
    ExtraSmall(1, 12),
    ;
}