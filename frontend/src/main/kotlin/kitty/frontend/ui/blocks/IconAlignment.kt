package kitty.frontend.ui.blocks

import androidx.compose.runtime.*

enum class IconAlignment {
    Default,
    Left,
    Right,
    ;

    companion object {
        val current: IconAlignment
            @Composable
            @ReadOnlyComposable
            get() = localIconAlignment.current
    }
}

private val localIconAlignment = staticCompositionLocalOf { IconAlignment.Default }

@Composable
fun ProvideIconAlignment(iconAlignment: IconAlignment, content: @Composable () -> Unit) {
    CompositionLocalProvider(localIconAlignment provides iconAlignment, content = content)
}