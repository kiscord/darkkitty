package kitty.frontend.ui.blocks

import androidx.compose.runtime.*

enum class IconSize {
    Default,
    Small,
    Medium,
    Large,
    ;

    companion object {
        val current: IconSize
            @Composable
            @ReadOnlyComposable
            get() = localIconSize.current
    }
}

private val localIconSize = staticCompositionLocalOf { IconSize.Default }

@Composable
fun ProvideIconSize(iconSize: IconSize, content: @Composable () -> Unit) {
    CompositionLocalProvider(localIconSize provides iconSize, content = content)
}