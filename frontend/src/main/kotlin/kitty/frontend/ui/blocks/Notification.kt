package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kotlinx.browser.*
import kotlinx.coroutines.*
import kotlinx.datetime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*
import kotlin.time.*
import kotlin.time.Duration.Companion.milliseconds


@Composable
fun Toast(
    toastAlignment: ToastAlignment = ToastAlignment.TopRight,
    durationMs: Long = Duration.INFINITE.inWholeMilliseconds,
    content: ContentBuilder<HTMLDivElement>
) {
    val dismissHandle = DismissHandle.current

    val duration = durationMs.milliseconds

    val timestamps: DismissHandle.TimeStamps?

    if (duration != Duration.INFINITE) {
        val startTime = remember { Clock.System.now() }
        val endTime = remember(startTime, duration) { startTime + duration }

        timestamps = DismissHandle.TimeStamps(start = startTime, end = endTime)

        LaunchedEffect(duration) {
            val now = Clock.System.now()
            val delayDuration = startTime + duration - now
            if (delayDuration > Duration.ZERO) {
                delay(delayDuration)
            }
            dismissHandle.dismiss()
        }
    } else {
        timestamps = null
    }

    Div(attrs = {
        classes("toast")
        when (toastAlignment) {
            ToastAlignment.TopLeft -> classes("top-left")
            ToastAlignment.TopCenter -> classes("top-center")
            ToastAlignment.TopRight -> classes("top-right")
            ToastAlignment.BottomLeft -> classes("bottom-left")
            ToastAlignment.BottomCenter -> classes("bottom-center")
            ToastAlignment.BottomRight -> classes("bottom-right")
        }
    }) {
        ProvideDismissHandle(object : DismissHandle {
            override val dismissTimestamps: DismissHandle.TimeStamps?
                get() = timestamps

            override fun dismiss() {
                dismissHandle.dismiss()
            }
        }) {
            content(this)
        }
    }
}

enum class ToastAlignment {
    TopLeft,
    TopCenter,
    TopRight,
    BottomLeft,
    BottomCenter,
    BottomRight,
    ;
}

@Composable
fun Notification(
    attrs: AttrBuilderContext<HTMLDivElement>? = null,
    content: ContentBuilder<HTMLDivElement>
) {
    val dismissHandle = DismissHandle.current

    Div(attrs = {
        classes("notification")
        attrs?.invoke(this)
    }) {
        Button(attrs = {
            classes("delete")
            onClick { dismissHandle.dismiss() }
        })
        content(this)
        val timestamps = dismissHandle.dismissTimestamps
        if (timestamps != null) {
            fun calculateProgress(): Double {
                val now = Clock.System.now().coerceIn(timestamps.start, timestamps.end)
                return 1.0 - (now - timestamps.start) / (timestamps.end - timestamps.start)
            }

            var progress by remember {
                mutableStateOf(calculateProgress())
            }

            fun callback() {
                val p = calculateProgress()
                progress = p
                if (p > 0.0) {
                    window.requestAnimationFrame { callback() }
                }
            }

            DisposableEffect(timestamps) {
                val handle = window.requestAnimationFrame { callback() }
                onDispose { window.cancelAnimationFrame(handle) }
            }

            Progress(attrs = {
                classes("progress", "dismiss-timer", "is-info")
                attr("max", "1")
                prop(setProgress, progress)
            })
        }
    }
}

internal val setProgress: (HTMLProgressElement, Double) -> Unit = { e, v ->
    if (v != e.value) {
        e.value = v
    }
}


interface ToastHost {
    fun submit(
        toastAlignment: ToastAlignment = ToastAlignment.TopRight,
        durationMs: Long = Duration.INFINITE.inWholeMilliseconds,
        content: ContentBuilder<HTMLDivElement>
    )

    companion object {
        val current: ToastHost
            @Composable
            @ReadOnlyComposable
            get() = LocalToastHost.current
    }
}

private val LocalToastHost = staticCompositionLocalOf<ToastHost> {
    throw IllegalStateException("Toast host not found in the current composition tree")
}

private class ToastEntry(
    val toastAlignment: ToastAlignment = ToastAlignment.TopRight,
    val durationMs: Long = Duration.INFINITE.inWholeMilliseconds,
    val content: ContentBuilder<HTMLDivElement>
)

private class ToastHostImpl(
    val toasts: MutableCollection<ToastEntry>
) : ToastHost {
    override fun submit(toastAlignment: ToastAlignment, durationMs: Long, content: ContentBuilder<HTMLDivElement>) {
        toasts += ToastEntry(toastAlignment, durationMs, content)
    }
}

@Composable
fun ToastController(block: @Composable () -> Unit) {
    val toasts = remember { mutableStateListOf<ToastEntry>() }
    val host = remember { ToastHostImpl(toasts) }

    CompositionLocalProvider(LocalToastHost provides host) {
        val toast = toasts.firstOrNull()
        if (toast != null) {
            ProvideDismissHandle({ toasts.remove(toast) }) {
                Toast(
                    toastAlignment = toast.toastAlignment,
                    durationMs = toast.durationMs,
                    content = toast.content,
                )
            }
        }
        block()
    }
}
