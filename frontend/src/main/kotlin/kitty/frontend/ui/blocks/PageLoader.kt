package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*

@Composable
fun PageLoader(isActive: Boolean, title: String? = null) {
    Div(attrs = {
        classes("pageloader")
        if (isActive) classes("is-active")
    }) {
        if (title != null) Span(attrs = { classes("title") }) { Text(title) }
    }
}