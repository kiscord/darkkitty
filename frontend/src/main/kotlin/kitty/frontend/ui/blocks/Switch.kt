package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*

@Composable
fun Switch(
    value: Boolean,
    onToggle: (Boolean) -> Unit,
    label: String? = null,
    isRounded: Boolean = true,
    isOutlined: Boolean = false,
    attrs: AttrBuilderContext<HTMLLabelElement>? = null,
) {
    Label(attrs = {
        classes("switch")
        if (isRounded) classes("is-rounded")
        if (isOutlined) classes("is-outlined")
        attrs?.invoke(this)
    }) {
        Input(InputType.Checkbox) {
            checked(value)
            onChange { onToggle(it.value) }
        }
        Span(attrs = {
            classes("check")
        })
        if (label != null) Span(attrs = {
            classes("control-label")
        }) {
            Text(label)
        }
    }
}