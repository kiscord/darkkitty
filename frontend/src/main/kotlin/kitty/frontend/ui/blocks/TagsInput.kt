package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.dom.*

data class Tag(
    val value: String,
    val text: String,
) {
    constructor(value: String) : this(value, value)
}

@Composable
fun TagsInput(
    tags: Collection<Tag>,
    selected: Collection<Tag> = emptySet(),
    placeholder: String? = null,
    onSelect: ((Tag) -> Unit)? = null,
    onDelete: ((Tag) -> Unit)? = null,
    onCreate: ((Tag) -> Unit)? = null,
) {
    Div(attrs = {
        classes("tags-input", "is-rounded")
    }) {
        tags.forEach { tag ->
            key(tag) {
                A(href = "#", attrs = {
                    classes("tag", "is-rounded")
                    if (tag in selected) classes("is-active")
                    attr("role", "button")
                    attr("data-value", tag.value)
                    if (onSelect != null) onClick { e ->
                        onSelect(tag)
                        e.stopPropagation()
                    }
                }) {
                    Text(tag.text)
                    if (onDelete != null) Button(attrs = {
                        classes("delete")
                        onClick { e ->
                            onDelete(tag)
                            e.stopPropagation()
                        }
                    })
                }
            }
        }

        var input by remember { mutableStateOf("") }

        fun tryToSubmitTag() {
            val tag = input.trim()
            if (tag.isNotBlank()) {
                onCreate?.invoke(Tag(tag))
                input = ""
            }
        }

        if (onCreate != null) Input(InputType.Text) {
            classes("input")
            attr("data-type", "tags")
            attr("enterkeyhint", "next")
            value(input)
            if (placeholder != null && input.isBlank() && tags.isEmpty()) {
                placeholder(placeholder)
            }
            onInput { e ->
                input = e.value
            }
            onKeyDown { e ->
                if (e.getNormalizedKey() == "Enter") {
                    e.preventDefault()
                    tryToSubmitTag()
                }
            }
            onFocusOut {
                tryToSubmitTag()
            }
        }
    }
}