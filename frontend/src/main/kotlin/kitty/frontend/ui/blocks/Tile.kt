package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun Tile(
    isAncestor: Boolean = false,
    isParent: Boolean = false,
    isChild: Boolean = false,
    isVertical: Boolean = false,
    attrs: AttrBuilderContext<HTMLDivElement>? = null,
    content: ContentBuilder<HTMLDivElement>? = null
) {
    Div(attrs = {
        classes("tile")
        if (isAncestor) classes("is-ancestor")
        if (isParent) classes("is-parent")
        if (isChild) classes("is-child")
        if (isVertical) classes("is-vertical")
        attrs?.invoke(this)
    }) {
        content?.invoke(this)
    }
}