package kitty.frontend.ui.blocks.navbar

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun NavBar(
    attrs: AttrBuilderContext<HTMLElement>? = null,
    content: ContentBuilder<HTMLElement>? = null,
) {
    Nav(attrs = {
        classes("navbar")
        attr("role", "navigation")
        attr("aria-label", "main-navigation")
        attrs?.invoke(this)
    }, content = content)
}