package kitty.frontend.ui.rich_text

import androidx.compose.runtime.*
import kitty.rich_text.*
import kotlinx.browser.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*
import org.jetbrains.compose.web.dom.Text as ComposeText

@Stable
sealed interface RichText : CharSequence {
    val children: Collection<RichText> get() = emptyList()

    fun rawTextTo(out: Appendable) {
        children.forEach { child ->
            child.rawTextTo(out)
        }
    }

    @Composable
    fun renderComposable() {
        children.forEach {
            it.renderComposable()
        }
    }

    fun renderToDOM(parent: Node) {
        children.forEach {
            it.renderToDOM(parent)
        }
    }

    @Stable
    data class Raw(val text: String) : RichText {
        override val length: Int get() = text.length
        override fun get(index: Int): Char = text[index]
        override fun subSequence(startIndex: Int, endIndex: Int): CharSequence = text.subSequence(startIndex, endIndex)

        override fun rawTextTo(out: Appendable) {
            out.append(text)
        }

        override fun toString(): String = text

        @Composable
        override fun renderComposable() {
            ComposeText(text)
        }

        override fun renderToDOM(parent: Node) {
            parent.appendChild(document.createTextNode(text))
        }
    }

    @Stable
    abstract class Styled(val text: RichText) : RichText {
        override val children: Collection<RichText> get() = listOf(text)
        override val length: Int get() = text.length
        override fun get(index: Int): Char = text[index]
        override fun subSequence(startIndex: Int, endIndex: Int): CharSequence = text.subSequence(startIndex, endIndex)

        override fun hashCode(): Int = text.hashCode()
        override fun equals(other: Any?): Boolean {
            if (other === this) return true
            if (other == null || this::class != other::class) return false
            return text == (other as Styled).text
        }

        override fun toString(): String = text.toString()
    }

    @Stable
    class Colored(
        text: RichText,
        val color: RichTextColor,
        val target: RichTextColorTarget,
    ) : Styled(text) {

        @Composable
        override fun renderComposable() {
            colorApplier(color, target) {
                super.renderComposable()
            }
        }

        override fun renderToDOM(parent: Node) {
            val colored = coloredNode(color, target, parent)
            super.renderToDOM(colored)
        }
    }

    @Stable
    class Compound(override val children: Collection<RichText>) : RichText {
        private val chunks by lazy(LazyThreadSafetyMode.NONE) {
            buildList {
                var offset = 0
                children.forEach { text ->
                    val length = text.length
                    add(offset until length to text)
                    offset += length
                }
            }
        }
        override val length: Int
            get() = children.sumOf { it.length }

        override fun get(index: Int): Char {
            val chunk = chunks.firstOrNull { index in it.first }
                ?: throw NoSuchElementException("Index $index out of range")
            return chunk.second[index - chunk.first.first]
        }

        // Pretty dumb and naive implementation, but who cares 🤷
        override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
            check(startIndex >= 0) { "startIndex must be a non negative" }
            check(startIndex <= endIndex) { "startIndex must be lesser or equals to endIndex" }
            check(endIndex <= length) { "endIndex must be lesser or equals to length" }
            if (startIndex == endIndex) return ""
            val firstChunk = chunks.indexOfFirst { startIndex in it.first }
            val subChunks = chunks.subList(firstChunk, chunks.size)
                .takeWhile { endIndex < it.first.first || endIndex in it.first }
            return buildString {
                subChunks.forEach { (range, text) ->
                    append(
                        text.subSequence(
                            maxOf(0, startIndex - range.first),
                            minOf(text.length, endIndex - range.last)
                        )
                    )
                }
            }
        }

        override fun hashCode(): Int {
            return children.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Compound) return false
            return children == other.children
        }

        override fun toString(): String = when (children.size) {
            0 -> ""
            1 -> children.first().toString()
            else -> children.joinToString(separator = "")
        }
    }

    @Stable
    class Italic(text: RichText) : Styled(text) {
        @Composable
        override fun renderComposable() {
            Span(attrs = {
                classes("italic")
            }) {
                super.renderComposable()
            }
        }

        override fun renderToDOM(parent: Node) {
            val italic = document.createElement("span")
            italic.className = "italic"
            super.renderToDOM(italic)
            parent.appendChild(italic)
        }
    }

    @Stable
    class Bold(text: RichText) : Styled(text) {
        @Composable
        override fun renderComposable() {
            Span(attrs = {
                classes("bold")
            }) {
                super.renderComposable()
            }
        }

        override fun renderToDOM(parent: Node) {
            val bold = document.createElement("span")
            bold.className = "bold"
            super.renderToDOM(bold)
            parent.appendChild(bold)
        }
    }

    @Stable
    class Faint(text: RichText) : Styled(text) {
        @Composable
        override fun renderComposable() {
            Span(attrs = {
                classes("faint")
            }) {
                super.renderComposable()
            }
        }

        override fun renderToDOM(parent: Node) {
            val faint = document.createElement("span")
            faint.className = "faint"
            super.renderToDOM(faint)
            parent.appendChild(faint)
        }
    }

    @Stable
    class Underline(text: RichText) : Styled(text) {
        @Composable
        override fun renderComposable() {
            Span(attrs = {
                classes("underline")
            }) {
                super.renderComposable()
            }
        }

        override fun renderToDOM(parent: Node) {
            val underline = document.createElement("span")
            underline.className = "underline"
            super.renderToDOM(underline)
            parent.appendChild(underline)
        }
    }

    object Empty : RichText {
        @Composable
        override fun renderComposable() = Unit
        override fun renderToDOM(parent: Node) = Unit

        override val length: Int get() = 0
        override fun get(index: Int): Char = throw IndexOutOfBoundsException("Text is empty")
        override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
            if (startIndex == 0 && endIndex == 0) return this
            throw IndexOutOfBoundsException("Text is empty")
        }

        override fun hashCode(): Int = 0
        override fun equals(other: Any?): Boolean = other === this
        override fun toString(): String = ""
    }

    companion object Emitter : RichTextEmitter<RichText> {
        override val empty: RichText get() = Empty
        override fun text(text: String): RichText = Raw(text)
        override fun foregroundColor(color: RichTextColor, content: RichText): RichText =
            Colored(content, color, RichTextColorTarget.Foreground)

        override fun backgroundColor(color: RichTextColor, content: RichText): RichText =
            Colored(content, color, RichTextColorTarget.Background)

        override fun join(parts: Collection<RichText>): RichText = Compound(parts.flatMap { text ->
            when (text) {
                is Compound -> text.children
                else -> listOf(text)
            }
        })

        override fun bold(content: RichText): RichText = Bold(content)
        override fun faint(content: RichText): RichText = Faint(content)
        override fun italic(content: RichText): RichText = Italic(content)

        override fun underline(content: RichText): RichText = Underline(content)

        override fun underlineColor(color: RichTextColor, content: RichText): RichText =
            Colored(content, color, RichTextColorTarget.Underline)
    }
}

fun RichText.rawText() = rawTextTo(StringBuilder())

@Composable
private fun colorApplier(color: RichTextColor, kind: RichTextColorTarget, content: @Composable () -> Unit) {
    when (color) {
        RichTextColor.Default -> content()

        is RichTextColor.Palette -> {
            Span(attrs = {
                classes("ansi-${kind.name.lowercase()}-${color.index}")
            }) {
                content()
            }
        }

        is RichTextColor.RGB -> {
            Span(attrs = {
                classes("ansi-${kind.name.lowercase()}-rgb")
                style {
                    when (kind) {
                        RichTextColorTarget.Background -> backgroundColor(color.toCSSColor())
                        RichTextColorTarget.Foreground -> color(color.toCSSColor())
                        RichTextColorTarget.Underline -> textDecorationColor(color.toCSSColor())
                    }
                }
            }) {
                content()
            }
        }
    }
}

private fun coloredNode(color: RichTextColor, kind: RichTextColorTarget, parent: Node): Node {
    when (color) {
        RichTextColor.Default -> return parent

        is RichTextColor.Palette -> {
            val colored = document.createElement("span") as HTMLSpanElement
            colored.className = "ansi-${kind.name.lowercase()}-${color.index}"
            parent.appendChild(colored)
            return colored
        }

        is RichTextColor.RGB -> {
            val colored = document.createElement("span") as HTMLSpanElement
            colored.className = "ansi-${kind.name.lowercase()}-rgb"
            when (kind) {
                RichTextColorTarget.Background -> {
                    colored.style.backgroundColor = color.toCSSColor().toString()
                }

                RichTextColorTarget.Foreground -> {
                    colored.style.color = color.toCSSColor().toString()
                }

                RichTextColorTarget.Underline -> {
                    colored.style.textDecorationColor = color.toCSSColor().toString()
                }
            }
            parent.appendChild(colored)
            return colored
        }
    }
}

