package kitty.frontend.ui.rich_text

import kitty.rich_text.*
import org.jetbrains.compose.web.css.*

fun RichTextColor.RGB.toCSSColor(): CSSColorValue =
    "#${color.toString(16).padStart(6, '0')}".unsafeCast<CSSColorValue>()

