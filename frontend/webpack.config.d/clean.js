(function (config) {
    const {CleanWebpackPlugin} = require("clean-webpack-plugin");

    config.plugins.push(new CleanWebpackPlugin());
})(config);