(function (config) {
    const HtmlWebpackPlugin = require('html-webpack-plugin');
    const path = require('path');

    config.plugins.push(new HtmlWebpackPlugin({
        filename: './index.html',
        template: path.resolve('../../../../frontend/src/main/web/index.html'),
        title: 'Kitty',
        chunks: ['main']
    }));
})(config);
