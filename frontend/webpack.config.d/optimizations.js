(function (config) {
    const TerserPlugin = require("terser-webpack-plugin");
    const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
    const JsonMinimizerPlugin = require("json-minimizer-webpack-plugin");
    const devMode = config.mode !== "production";

    if (devMode) {
        config.output.filename = "js/[name].js";
    } else {
        config.output.filename = "js/[contenthash].js"
    }

    config.performance = {
        ...config.performance,
        maxEntrypointSize: 10240000,
        maxAssetSize: 10240000,
    };

    config.optimization = {
        ...config.optimization,
        minimize: !devMode,
        minimizer: [
            new TerserPlugin({
                extractComments: false,
            }),
            new CssMinimizerPlugin(),
            new JsonMinimizerPlugin({
              test: /\.(json|webmanifest)(\?.*)?$/i
            }),
        ]
    };

    config.optimization.splitChunks = {
        hidePathInfo: true,
        cacheGroups: {
            kotlin: {
                test: /kotlin[\\/]kotlin_(kotlin\.js|org_jetbrains_kotlin)/,
                chunks: 'all',
                name: 'kotlin',
                priority: 30,
            },
            compose: {
                test: /kotlin[\\/]kotlin_(androidx_compose|org_jetbrains_compose)/,
                chunks: 'all',
                name: 'compose',
                priority: 20,
            },
            defaultVendors: {
                test: /[\\/]node_modules[\\/]/,
                name: 'vendor',
                chunks: 'all',
            },
            default: {
                minChunks: 2,
                reuseExistingChunk: true,
            },
        },
    };
})(config);
