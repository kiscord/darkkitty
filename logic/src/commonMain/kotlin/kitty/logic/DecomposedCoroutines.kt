package kitty.logic

import com.arkivanov.essenty.lifecycle.*
import kotlin.coroutines.*
import kotlinx.coroutines.*

fun CoroutineScope(context: CoroutineContext, lifecycle: Lifecycle): CoroutineScope {
    val scope = CoroutineScope(context)
    lifecycle.doOnDestroy(scope::cancel)
    return scope
}

fun LifecycleOwner.coroutineScope(context: CoroutineContext = EmptyCoroutineContext): CoroutineScope =
    CoroutineScope(context, lifecycle)
