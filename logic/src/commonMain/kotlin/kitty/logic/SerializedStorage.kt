package kitty.logic

import kotlinx.coroutines.flow.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*

expect class SerializedStorage<T : Any>(
    key: String,
    serializer: KSerializer<T>,
    format: StringFormat = Json,
) {
    val value: T?
    val flow: StateFlow<T?>

    fun clear()

    fun set(value: T)
}