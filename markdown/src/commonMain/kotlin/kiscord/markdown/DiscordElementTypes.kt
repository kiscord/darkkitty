package kiscord.markdown

import org.intellij.markdown.IElementType
import org.intellij.markdown.MarkdownElementType
import kotlin.jvm.JvmField

object DiscordTokenTypes {
    @JvmField
    val TILDE: IElementType = MarkdownElementType("~", true)

    @JvmField
    val UNDERLINE_EMPH: IElementType = MarkdownElementType("_", true)

    @JvmField
    val PIPE: IElementType = MarkdownElementType("|", true)

    @JvmField
    val DISCORD_AUTOLINK: IElementType = MarkdownElementType("DISCORD_AUTOLINK", true)
}

object DiscordElementTypes {
    @JvmField
    val STRIKETHROUGH: IElementType = MarkdownElementType("STRIKETHROUGH")

    @JvmField
    val UNDERLINE: IElementType = MarkdownElementType("UNDERLINE")

    @JvmField
    val SPOILER: IElementType = MarkdownElementType("SPOILER")
}
