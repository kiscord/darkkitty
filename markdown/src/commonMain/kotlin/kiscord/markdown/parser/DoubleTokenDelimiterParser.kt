package kiscord.markdown.parser

import org.intellij.markdown.IElementType
import org.intellij.markdown.parser.sequentialparsers.DelimiterParser
import org.intellij.markdown.parser.sequentialparsers.SequentialParser
import org.intellij.markdown.parser.sequentialparsers.TokensCache
import org.intellij.markdown.parser.sequentialparsers.impl.EmphStrongDelimiterParser

open class DoubleTokenDelimiterParser(
    val token: IElementType,
    val element: IElementType
) : DelimiterParser() {
    override fun scan(tokens: TokensCache, iterator: TokensCache.Iterator, delimiters: MutableList<Info>): Int {
        if (iterator.type != token) {
            return 0
        }
        var stepsToAdvance = 1
        var rightIterator = iterator
        for (index in 0 until maxAdvance) {
            if (rightIterator.rawLookup(1) != token) {
                break
            }
            rightIterator = rightIterator.advance()
            stepsToAdvance += 1
        }
        val (canOpen, canClose) = canOpenClose(tokens, iterator, rightIterator, canSplitText = true)
        for (index in 0 until stepsToAdvance) {
            val info = Info(
                tokenType = token,
                position = iterator.index + index,
                length = 0,
                canOpen = canOpen,
                canClose = canClose,
                marker = '~'
            )
            delimiters.add(info)
        }
        return stepsToAdvance
    }

    override fun process(
        tokens: TokensCache,
        iterator: TokensCache.Iterator,
        delimiters: MutableList<Info>,
        result: SequentialParser.ParsingResultBuilder
    ) {
        var shouldSkipNext = false
        for (index in delimiters.indices.reversed()) {
            if (shouldSkipNext) {
                shouldSkipNext = false
                continue
            }
            val opener = delimiters[index]
            if (opener.tokenType != token || opener.closerIndex == -1) {
                continue
            }
            shouldSkipNext = EmphStrongDelimiterParser.areAdjacentSameMarkers(delimiters, index, opener.closerIndex)
            val closer = delimiters[opener.closerIndex]
            if (shouldSkipNext) {
                val node = SequentialParser.Node(
                    range = opener.position - 1..closer.position + 2,
                    type = element
                )
                result.withNode(node)
            }
        }
    }

}