package kiscord.markdown.parser

import org.intellij.markdown.IElementType
import org.intellij.markdown.MarkdownElementTypes
import org.intellij.markdown.MarkdownTokenTypes
import kiscord.markdown.DiscordElementTypes
import kiscord.markdown.DiscordTokenTypes
import org.intellij.markdown.parser.sequentialparsers.DelimiterParser
import org.intellij.markdown.parser.sequentialparsers.SequentialParser
import org.intellij.markdown.parser.sequentialparsers.TokensCache
import org.intellij.markdown.parser.sequentialparsers.impl.EmphStrongDelimiterParser

class EmphStrongUnderlineDelimiterParser: DelimiterParser() {
    private val IElementType?.isEmph: Boolean
        get() = this == MarkdownTokenTypes.EMPH || this == DiscordTokenTypes.UNDERLINE_EMPH

    override fun scan(tokens: TokensCache, iterator: TokensCache.Iterator, delimiters: MutableList<Info>): Int {
        if (!iterator.type.isEmph) {
            return 0
        }
        var stepsToAdvance = 1
        var rightIterator = iterator
        val marker = getType(rightIterator)
        for (index in 0 until maxAdvance) {
            if (!rightIterator.rawLookup(1).isEmph || getType(rightIterator.advance()) != marker) {
                break
            }
            rightIterator = rightIterator.advance()
            stepsToAdvance += 1
        }
        val (canOpen, canClose) = canOpenClose(tokens, iterator, rightIterator, canSplitText = marker == '*')
        for (index in 0 until stepsToAdvance) {
            val info = Info(
                tokenType = iterator.type!!,
                position = iterator.index + index,
                length = stepsToAdvance,
                canOpen = canOpen,
                canClose = canClose,
                marker = marker
            )
            delimiters.add(info)
        }
        return stepsToAdvance
    }

    override fun process(
        tokens: TokensCache,
        iterator: TokensCache.Iterator,
        delimiters: MutableList<Info>,
        result: SequentialParser.ParsingResultBuilder
    ) {
        var isStrong = false
        for (index in delimiters.indices.reversed()) {
            if (isStrong) {
                isStrong = false
                continue
            }
            val opener = delimiters[index]
            if (!opener.tokenType.isEmph || opener.closerIndex == -1) {
                continue
            }
            isStrong = EmphStrongDelimiterParser.areAdjacentSameMarkers(delimiters, index, opener.closerIndex)
            val closer = delimiters[opener.closerIndex]
            val node = when {
                isStrong -> SequentialParser.Node(
                    range = opener.position - 1..closer.position + 2,
                    type = when (opener.tokenType) {
                        DiscordTokenTypes.UNDERLINE_EMPH -> DiscordElementTypes.UNDERLINE
                        else -> MarkdownElementTypes.STRONG
                    }
                )
                else -> SequentialParser.Node(
                    range = opener.position..closer.position + 1,
                    type = MarkdownElementTypes.EMPH
                )
            }
            result.withNode(node)
        }
    }
}