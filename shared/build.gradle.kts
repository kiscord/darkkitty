import com.codingfeline.buildkonfig.compiler.*
import com.codingfeline.buildkonfig.gradle.*
import org.jetbrains.kotlin.util.capitalizeDecapitalize.*

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    alias(libs.plugins.buildkonfig)
    alias(libs.plugins.properties)
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

fun JavaToolchainSpec.kittyToolchain() {
    languageVersion.set(JavaLanguageVersion.of(17))
}

kotlin {
    jvm {
        withJava()

        compilations.configureEach {
            kotlinOptions {
                jvmTarget = "17"
                freeCompilerArgs = freeCompilerArgs + "-Xcontext-receivers"
            }
        }
    }

    js {
        useCommonJs()
        browser()
    }

    sourceSets.configureEach {
        languageSettings {
            optIn("kotlin.RequiresOptIn")
            progressiveMode = true
        }
    }

    jvmToolchain {
        (this as JavaToolchainSpec).kittyToolchain()
    }
}

java {
    toolchain {
        kittyToolchain()
    }
}

dependencies {
    commonMainApi(kiscord.kiscord.core)
    commonMainApi(kiscord.kotlinx.datetime)
    commonMainApi(kiscord.kotlinx.serialization.core)
    commonMainApi(kiscord.kotlinx.serialization.json)
    commonMainApi(kiscord.ktor.client.core)
    commonMainApi(libs.kodein.di)
    commonMainApi("io.ktor:ktor-client-auth")
    commonMainApi("io.ktor:ktor-client-content-negotiation")
    commonMainApi("io.ktor:ktor-client-serialization")
    commonMainApi("io.ktor:ktor-resources")
    commonMainApi("io.ktor:ktor-serialization-kotlinx-cbor")
    commonMainApi("io.ktor:ktor-serialization-kotlinx-json")
    commonMainApi("org.jetbrains.kotlinx:kotlinx-serialization-cbor")
    "jvmMainImplementation"(kiscord.logback.classic)
    "jvmMainImplementation"(libs.jansi)
}

buildkonfig {
    packageName = "kitty"
    exposeObjectWithName = "BuildConfig"

    fun isModuleEnabled(moduleName: String): Boolean = when {
        hasProperty("kitty.module.$moduleName") -> project.property("kitty.module.$moduleName").toString().toBoolean()
        else -> true
    }

    fun TargetConfigDsl.addModule(moduleName: String) {
        val isEnabled = isModuleEnabled(moduleName)
        buildConfigField(
            FieldSpec.Type.BOOLEAN, "MODULE_${moduleName.toUpperCaseAsciiOnly()}",
            isEnabled.toString(), const = true
        )
    }

    defaultConfigs {
        buildConfigField(FieldSpec.Type.STRING, "NAME", rootProject.name.capitalize(), const = true)
        buildConfigField(FieldSpec.Type.STRING, "VERSION", project.version.toString(), const = true)
        buildConfigField(FieldSpec.Type.STRING, "VCS_URL", "https://gitlab.com/kiscord/kitty", const = true)
        buildConfigField(FieldSpec.Type.BOOLEAN, "DISABLE_CBOR", rootProject.property("kitty.cbor.disable").toString(), const = true)
        addModule("faq")
        addModule("permissions")
        addModule("pasta")
    }
}
