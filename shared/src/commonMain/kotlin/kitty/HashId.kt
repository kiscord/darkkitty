package kitty

import kotlinx.serialization.*
import kotlin.jvm.*
import kotlin.random.*

@JvmInline
@Serializable
value class HashId(val hash: String) : Comparable<HashId> {
    override fun compareTo(other: HashId): Int = hash.compareTo(other.hash)

    override fun toString(): String = hash

    companion object {
        const val DEFAULT_HASH_SIZE = 16
        private const val ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

        fun generate(hashSize: Int = DEFAULT_HASH_SIZE, random: Random = Random): HashId {
            return HashId(buildString(hashSize) {
                repeat(hashSize) {
                    append(ALPHABET.random(random))
                }
            })
        }
    }
}
