package kitty

import kotlinx.serialization.*

@Serializable
data class StatsResponse(
    val guilds: Long,
    val users: Long,
)