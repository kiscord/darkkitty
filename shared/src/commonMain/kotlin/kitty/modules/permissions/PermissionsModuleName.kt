package kitty.modules.permissions

import kitty.resources.*

val PermissionsModuleName = ModuleScoped.Name("module-permissions")
