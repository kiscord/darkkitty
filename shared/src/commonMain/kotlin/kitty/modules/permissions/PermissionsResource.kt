package kitty.modules.permissions

import kitty.modules.faq.*
import kitty.resources.*
import io.ktor.resources.*
import kiscord.api.*
import kotlinx.serialization.Serializable

@Serializable
@Resource("/permissions")
data class PermissionsResource(override val parent: GuildScopedResource) : HasParentResource, ModuleScoped {
    constructor(guildId: Snowflake) : this(GuildScopedResource(guildId = guildId))

    override val moduleName: ModuleScoped.Name get() = PermissionsModuleName
}
