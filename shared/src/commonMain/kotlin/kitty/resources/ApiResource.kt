package kitty.resources

import io.ktor.resources.*
import kiscord.api.*
import kotlinx.serialization.*

@Serializable
@Resource("/api")
class ApiResource {
    @Serializable
    @Resource("/stats")
    data class Stats(
        override val parent: ApiResource = ApiResource(),
    ) : HasParentResource

    @Serializable
    @Resource("/users")
    data class Users(
        override val parent: ApiResource = ApiResource(),
    ) : HasParentResource {
        @Serializable
        @Resource("/@me")
        data class Me(
            override val parent: Users = Users(),
        ) : HasParentResource {
            @Serializable
            @Resource("/guilds")
            data class Guilds(
                override val parent: Me = Me(),
            ) : HasParentResource
        }

        @Serializable
        @Resource("/@bot")
        data class Bot(
            override val parent: Users = Users(),
        ) : HasParentResource

        @Serializable
        @Resource("/{userId}")
        data class Info(
            override val parent: Users = Users(),
            val userId: Snowflake,
        ) : HasParentResource
    }

    override fun toString(): String = "ApiResource"
}