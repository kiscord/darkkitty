package kitty.resources

import io.ktor.resources.*
import kiscord.api.*
import kotlinx.serialization.*


@Serializable
@Resource("/guild/{guildId}")
data class GuildScopedResource(
    override val parent: ApiResource = ApiResource(),
    override val guildId: Snowflake,
) : GuildScoped, HasParentResource