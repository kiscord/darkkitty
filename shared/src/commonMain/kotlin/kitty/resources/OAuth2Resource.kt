package kitty.resources

import io.ktor.resources.*
import kiscord.api.*
import kotlinx.serialization.*

@Serializable
@Resource("/oauth2")
class OAuth2Resource(
    override val parent: ApiResource = ApiResource()
) : HasParentResource {
    @Serializable
    @Resource("/login/{provider}")
    data class Login(
        override val parent: OAuth2Resource = OAuth2Resource(),
        val provider: String,
    ) : HasParentResource

    @Serializable
    @Resource("/callback/{provider}")
    data class Callback(
        override val parent: OAuth2Resource = OAuth2Resource(),
        val provider: String,
    ) : HasParentResource

    @Serializable
    @Resource("/check")
    data class Check(
        override val parent: OAuth2Resource = OAuth2Resource(),
    ) : HasParentResource

    @Serializable
    @Resource("/refresh")
    data class Refresh(
        override val parent: OAuth2Resource = OAuth2Resource(),
    ) : HasParentResource

    override fun toString(): String = "OAuth2Resource"
}