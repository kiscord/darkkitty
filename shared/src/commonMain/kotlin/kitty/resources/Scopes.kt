package kitty.resources;

import kiscord.api.*
import kotlin.jvm.*
import kotlin.reflect.*

interface HasParentResource {
    val parent: Any
}

@Suppress("UNCHECKED_CAST")
fun <T : Any> HasParentResource.of(type: KClass<in T>): T {
    var resource: Any = this
    do {
        if (type.isInstance(resource)) {
            return resource as T
        }
        if (resource !is HasParentResource)
            throw IllegalArgumentException("Could not find requested parent resource of type $type for resource $this")
        resource = resource.parent
    } while (true)
}

inline fun <reified T : Any> HasParentResource.of(): T = of(T::class)

interface GuildScoped {
    val guildId: Snowflake
}

interface ModuleScoped {
    val moduleName: Name

    @JvmInline
    value class Name(val name: String)
}