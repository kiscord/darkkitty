package kitty

actual interface AutoCloseable {
    actual fun close()
}
